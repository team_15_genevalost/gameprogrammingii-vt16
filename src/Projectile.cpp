#include "Projectile.hpp"

Projectile::Projectile(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, float angle, float ttl, float speed, float damage, sf::Vector2f origin) : Entity(sprite, zBuffer, position, angle, origin)
{
	Create(ttl, speed, damage);
}

Projectile::Projectile(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, Origin origin, float angle, float ttl, float speed, float damage) : Entity(sprite, zBuffer, position, origin, angle)
{
	Create(ttl, speed, damage);
}

Projectile::Projectile(sf::Sprite* sprite, float zBuffer, float x, float y, float angle, float ttl, float speed, float damage, float originX, float originY) : Entity(sprite, zBuffer, x, y, angle, originX, originY)
{
	Create(ttl, speed, damage);
}

Projectile::Projectile(sf::Sprite* sprite, float zBuffer, float x, float y, Origin origin, float angle, float ttl, float speed, float damage) : Entity(sprite, zBuffer, x, y, origin, angle)
{
	Create(ttl, speed, damage);
}

int Projectile::GetDamage()
{
	return damage;
}

bool Projectile::IsDestroyed()
{
	return isDestroyed;
}

void Projectile::Update(float deltaTime)
{
	Entity::Update(deltaTime);
	float sine = sin(my::math::DegToRad(sprite->getRotation()));
	float cosine = cos(my::math::DegToRad(sprite->getRotation()));
	Move(cosine * deltaTime * speed, sine * deltaTime * speed);
	ttl -= deltaTime;
	if (ttl <= 0)
		isDestroyed = true;
}

void Projectile::Create(float ttl, float speed, float damage)
{
	if (ttl == .0f)
		ttl = 5.0f;
	if (speed == .0f)
		speed = 2000.0f;
	if (damage == .0f)
		damage = 10.0f;

	this->ttl = ttl;
	this->speed = speed;
	this->damage = damage;
}
