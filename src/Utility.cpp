#include "Utility.hpp"

float my::vmath::DotProduct(float x1, float y1, float x2, float y2)
{
	return x1 * x2 + y1* y2;
}

float my::vmath::Distance(float x1, float y1, float x2, float y2)
{
	return GetLength(x1 - x2, y1 - y2);
}

void my::vmath::Normalize(float* x, float* y)
{
	float length = GetLength(*x, *y);
	if (length == 0)
	{
		*x = 0;
		*y = 0;
		return;
	}
	*x /= length;
	*y /= length;
}

void my::vmath::GetUnit(float x, float y, float* targetX, float* targetY)
{
	Normalize(&x, &y);
	*targetX = x;
	*targetY = y;
}

float my::vmath::GetLength(float x, float y)
{
	return sqrt(x * x + y * y);
}

void my::vmath::RotateAroundOrigin(float* x, float* y, float angle)
{
	float sine = sin(angle);
	float cosine = cos(angle);
	float newX = *x * cosine - *y * sine;
	float newY = *x * sine + *y * cosine;
	*x = newX;
	*y = newY;
}

void my::vmath::RotateAroundPoint(float pointX, float pointY, float* x, float* y, float angle)
{
	float newX = *x - pointX;
	float newY = *y - pointY;
	RotateAroundOrigin(&newX, &newY, angle);
	*x = newX + pointX;
	*y = newY + pointY;
}

float my::vmath::ScalarProjection(float pointX, float pointY, float axisX, float axisY)
{
	return DotProduct(pointX, pointY, axisX, axisY) / GetLength(axisX, axisY);
}

void my::vmath::VectorProjection(float pointX, float pointY, float axisX, float axisY, float * targetX, float * targetY)
{
	float val = DotProduct(pointX, pointY, axisX, axisY) / DotProduct(axisX, axisY, axisX, axisY);
	*targetX = val * axisX;
	*targetY = val * axisY;
}

float my::math::Lerp(float v0, float v1, float t)
{
	return v0 * (1 - t) + v1 * t;
}

float my::math::Wrap(float min, float max, float val)
{
	Wrap(min, max, &val);
	return val;
}

void my::math::Wrap(float min, float max, float* val)
{
	while (*val >= max)
	{
		*val -= (max - min);
	}
	while (*val < min)
	{
		*val += (max - min);
	}
}

float my::math::Clip(float min, float max, float val)
{
	Clip(min, max, &val);
	return val;
}

void my::math::Clip(float min, float max, float* val)
{
	if (*val < min)
		*val = min;
	if (*val > max)
		*val = max;
}

float my::math::RadToDeg(float radians)
{
	return (radians / M_PI) * 180.0f;
}

float my::math::DegToRad(float degrees)
{
	return (degrees / 180) * M_PI;
}

int my::math::Sign(float val)
{
	if (val == 0)
		return 0;
	else
		return val > 0 ? 1 : -1;
}

bool my::math::HasOverlap(float minA, float maxA, float minB, float maxB)
{
	if (Contains(minA, maxA, minB) || Contains(minA, maxA, maxB))
		return true;
	return false;
}

bool my::math::Contains(float min, float max, float val)
{
	if (val > min && val < max)
		return true;
	return false;
}

float my::math::fRand(float min, float max)
{
	float random = (float)rand() / (float)RAND_MAX;
	random = random * (max - min) + min;
	return random;
}

float my::math::fRand(float range)
{
	float random = (float)rand() / (float)RAND_MAX;
	random = random * range;
	return random;
}
