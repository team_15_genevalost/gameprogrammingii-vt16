#include "..\include\EndState.hpp"

EndState::EndState(bool Win)
{
	checkWin = Win;
}

EndState::~EndState()
{
}

void EndState::Enter(sf::RenderWindow * window)
{
	this->window = window;
	
	scaleX = static_cast<float>(WindowSizeX) / static_cast<float>(window->getSize().x);
	scaleY = static_cast<float>(WindowSizeY) / static_cast<float>(window->getSize().y);
	
	view.setSize(WindowSizeX, WindowSizeY);
	view.setCenter(WindowSizeX / 2, WindowSizeY / 2);
	window->setView(view);
	spriteManager = new SpriteManager();

	LossCondition = spriteManager->CreateSprite("..\\resources\\GUI_Loss.png");
	VictoryCondition = spriteManager->CreateSprite("..\\resources\\GUI_Victory.png");
	menuExit = spriteManager->CreateSprite("..\\resources\\menuExit.png");
	reticle = spriteManager->CreateSprite("..\\resources\\reticle.png");
	returnToMenu = spriteManager->CreateSprite("..\\resources\\returnToMenu2.png");
	enterInitials = spriteManager->CreateSprite("..\\resources\\Menuhighscore\\initials.png");

	VictoryCondition->setScale(view.getSize().x / VictoryCondition->getLocalBounds().width, view.getSize().y / VictoryCondition->getLocalBounds().height);
	LossCondition->setScale(view.getSize().x / LossCondition->getLocalBounds().width, view.getSize().y / LossCondition->getLocalBounds().height);
	menuExit->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	enterInitials->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	returnToMenu->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	
	reticle->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	reticle->setOrigin(reticle->getLocalBounds().width / 2, reticle->getLocalBounds().height / 2);


	enterInitials->setPosition(0.1500f * WindowSizeX, 0.4000f * WindowSizeY);

	returnToMenu->setPosition(0.7212f * WindowSizeX, 0.5755f * WindowSizeY);
	menuExit->setPosition(0.677f * WindowSizeX, 0.6481f * WindowSizeY);

	mouse = sf::Mouse();
	window->setMouseCursorVisible(false);
	mousePosition = new sf::Vector2f();
	drawManager = new DrawManager(window);
}

bool EndState::Update(float deltaTIme)
{
	RECT rect = RECT();
	POINT point = POINT();
	sf::WindowHandle windowHandle = window->getSystemHandle();
	GetClientRect(windowHandle, &rect);
	ClientToScreen(windowHandle, &point);

	reticle->setPosition(sf::Vector2f((mouse.getPosition(*window).x) * scaleX, (mouse.getPosition(*window).y) * scaleY));

	if (returnToMenu->getGlobalBounds().contains(reticle->getPosition()))
	{
		returnToMenu->setColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			return false;
		}
	}
	else
	{
		returnToMenu->setColor(sf::Color::White);
	}

	if (menuExit->getGlobalBounds().contains(reticle->getPosition()))
	{
		menuExit->setColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			window->close();
		}
	}
	else
	{
		menuExit->setColor(sf::Color::White);
	}

	return true;
}

void EndState::Exit()
{
}

void EndState::Draw()
{
	window->clear(sf::Color::Blue);
	if (checkWin == true)
	{
		window->draw(*VictoryCondition);
	}
	else
	{
		window->draw(*LossCondition);
	}
	window->draw(*returnToMenu);
	window->draw(*enterInitials);
	window->draw(*menuExit);
	window->draw(*reticle);
	window->display();
}

IState * EndState::NextState()
{
	state = new MenuState();

	return state;
}
