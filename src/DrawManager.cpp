#include "DrawManager.hpp"

DrawManager::DrawManager(sf::RenderWindow* window)
{
	this->window = window;
}

void DrawManager::DrawCall(sf::Sprite* sprite, float zBuffer)
{
	buffer.push_back(std::pair<float, sf::Sprite*>(zBuffer, sprite));
}

void DrawManager::Draw(sf::Drawable* drawable, sf::RenderTexture* renderTexture)
{
	if (renderTexture != nullptr)
	{
		renderTexture->draw(*drawable);
		return;
	}
	window->draw(*drawable);
}

void DrawManager::Present(Camera* camera, sf::Color color, sf::RenderTexture* renderTexture)
{
	std::sort(buffer.begin(), buffer.end());
	if (renderTexture != nullptr)
	{
		renderTexture->setView(camera->GetCameraView());
		for (int i = 0; i < buffer.size(); i++)
		{
			if (camera->GetCameraRect().intersects(buffer[i].second->getGlobalBounds()))
			{
				if (buffer[i].second->getColor() != sf::Color::White)
				{
					renderTexture->draw(*buffer[i].second);
				}
				else
				{
					sf::Color temp = buffer[i].second->getColor();
					buffer[i].second->setColor(color);
					renderTexture->draw(*buffer[i].second);
					buffer[i].second->setColor(temp);
				}
			}
		}
		return;
	}
	window->setView(camera->GetCameraView());
	for (int i = 0; i < buffer.size(); i++)
	{
		if (camera->GetCameraRect().intersects(buffer[i].second->getGlobalBounds()))
		{
			if (buffer[i].second->getColor() != sf::Color::White)
			{
				window->draw(*buffer[i].second);
			}
			else
			{
				sf::Color temp = buffer[i].second->getColor();
				buffer[i].second->setColor(color);
				window->draw(*buffer[i].second);
				buffer[i].second->setColor(temp);
			}
		}
	}
}

void DrawManager::Present(sf::Color color, sf::RenderTexture* renderTexture)
{
	std::sort(buffer.begin(), buffer.end());
	if (renderTexture != nullptr)
	{
		for (int i = 0; i < buffer.size(); i++)
		{
			if (buffer[i].second->getColor() != sf::Color::White)
			{
				renderTexture->draw(*buffer[i].second);
			}
			else
			{
				sf::Color temp = buffer[i].second->getColor();
				buffer[i].second->setColor(color);
				renderTexture->draw(*buffer[i].second);
				buffer[i].second->setColor(temp);
			}
		}
		return;
	}
	for (int i = 0; i < buffer.size(); i++)
	{
		if (buffer[i].second->getColor() != sf::Color::White)
		{
			window->draw(*buffer[i].second);
		}
		else
		{
			sf::Color temp = buffer[i].second->getColor();
			buffer[i].second->setColor(color);
			window->draw(*buffer[i].second);
			buffer[i].second->setColor(temp);
		}
	}
}

void DrawManager::ClearBuffer()
{
	buffer.clear();
}
