#include "HighScore.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>

HighScore::HighScore(int el)
{
	int i;
	nEl = el;
	v = new score_t*[nEl];

	for (i = 0; i<nEl; i++) {
		v[i] = new score_t;
		v[i]->name = "Noname";
		v[i]->score = 0;
		v[i]->free = false; // static name
	}
}

HighScore::~HighScore()
{
	int i;

	for (i = 0; i<10; i++) {
		if (v[i]->free) { // delete if dyn mem
			delete[] v[i]->name;
		}
	}
}

void HighScore::add(std::string name, int score)
{
	HighScoreItem* item = new HighScoreItem();
	item->name = name;
	item->score = score;

	//L�gg in p� r�tt plats i listan
	//highscoreItems.push_back(item);


	score_t *tmp;
	
	bool scoreAdded = false;
	for (int i = 0; i < highscoreItems.size(); i++)
	{
		if (highscoreItems[i]->score > score)
		{
			continue;
		}

		highscoreItems.insert(highscoreItems.begin() + i, item);
		scoreAdded = true;
		break;
	}

	if (!scoreAdded)
	{
		highscoreItems.push_back(item);
	}

	while (highscoreItems.size() > 10)
	{
		highscoreItems.pop_back();
	}

	int rawr = 0;
	UpdateFile();
	//int i, j;
	

	//for (i = 0; i<nEl; i++) {
	//	if (score > v[i]->score) {
	//		tmp = v[nEl - 1]; // reuse the last score element
	//						  // move the scores 1 step down
	//		for (j = nEl - 1; j>i; j--) {
	//			v[j] = v[j - 1];
	//		}

	//		v[i] = tmp;
	//		// assign the new values
	//		v[i]->score = score;
	//		if (v[i]->free) {
	//			delete[] v[i]->name;
	//		}

	//		v[i]->free = true;
	//		v[i]->name = new char[std::strlen(name) + 1];
	//		strcpy_s(v[i]->name, 4, name);
	//		break;
	//	}
	//}
}
void HighScore::LoadHighscores()
{
	highscoreItems.clear();
	std::ifstream stream;
	stream.open("..\\resources\\level1\\Highscore\\semih.txt");

	if (stream.is_open())
	{
		std::string line;
		
		while (stream.good())
		{
			std::getline(stream, line);

			if (line.empty())
			{
				break;
			}
			int spaceIndex = line.find(' ');
			std::string name = line.substr(0, spaceIndex);
			std::string score = line.substr(spaceIndex + 1, line.size());

			HighScoreItem* item = new HighScoreItem();
			item->name = name;
			item->score = std::stoi(score);

			highscoreItems.push_back(item);
		}
	}

	stream.close();
}
void HighScore::UpdateFile()
{
	std::ofstream stream;
	stream.open("..\\resources\\level1\\Highscore\\semih.txt");
	

	for (int i = 0; i < highscoreItems.size(); i++)
	{		
		int tmp;
		stream << highscoreItems[i]->name;
		stream << " ";
		stream << highscoreItems[i]->score;
		stream << "\n";
	}

	stream.close();
}

void HighScore::print(std::ostream &out)
{
	int i;

	for (i = 0; i<nEl; i++) {
		out << v[i]->name << ": " << v[i]->score << std::endl;
	}
}