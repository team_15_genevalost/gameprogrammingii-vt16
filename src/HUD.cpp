#include "HUD.hpp"

HUD::HUD(SpriteManager* spriteManager, float sizeX, float sizeY)
{
	sf::FloatRect rect;
	rect.top = 0;
	rect.left = 0;
	rect.width = sizeX;
	rect.height = sizeY;
	HUDView = sf::View(rect);
	frameShade = spriteManager->CreateSprite("..\\resources\\Frame.png");
	float scaleX = sizeX / frameShade->getLocalBounds().width;
	float scaleY = sizeY / frameShade->getLocalBounds().height;
	frameShade->setScale(scaleX, scaleY);
	frame = spriteManager->CreateSprite("..\\resources\\HUDframe.png");
	frame->setScale(scaleX, scaleY);
	centerBox = spriteManager->CreateSprite("..\\resources\\HUDcenter.png");
	centerBox->setOrigin(centerBox->getLocalBounds().width / 2, centerBox->getLocalBounds().height / 2);
	centerBox->setPosition(HUDView.getCenter());
	centerBox->setScale(1.25f, 1.22f);
	textScroll = spriteManager->CreateSprite("..\\resources\\HUDtext.png", 5, 1, 5.0f);
	textScroll->setPosition(50 * scaleX, 55 * scaleY);
	textScroll->setScale(scaleX, scaleY);
	reticle = spriteManager->CreateSprite("..\\resources\\reticle.png");
	reticle->setOrigin(reticle->getLocalBounds().width / 2, reticle->getLocalBounds().height / 2);
	frameShade->setColor(sf::Color::Black);

	clockFont.loadFromFile("..\\resources\\DS-DIGI.TTF");
	clockText = sf::Text("Test", clockFont);
	clockText.setPosition(65 * scaleX, 625 * scaleY);
	clockText.setColor(sf::Color::White);
	clockText.setScale(scaleX, scaleY);

	centerBoxPosition = sf::Vector2f(0, 0);
	centerBoxMoveSpeed = 2000.0f;

	clock = 0.0f;

	debugBar = new sf::RectangleShape();
	debugBar->setSize(sf::Vector2f(sizeX, 100));
	debugBar->setPosition(0, sizeY - debugBar->getSize().y);
	debugBar->setFillColor(sf::Color(0, 0, 0, 128));

	debugFont.loadFromFile("..\\resources\\CODE Bold.otf");
	debugFPS = sf::Text("Test", debugFont);
	debugFPS.setPosition(25, sizeY - debugBar->getSize().y + 20);
	debugFPS.setColor(sf::Color::Yellow);

	lastFPSSize = 20;
	lastFPS = new int[lastFPSSize];
	for (int i = 0; i < lastFPSSize; i++)
	{
		lastFPS[i] = 0;
	}
}

void HUD::Update(float deltaTime)
{
	textScroll->Update(deltaTime);

	clock += deltaTime;
	int hours = clock / 3600;
	int minutes = (clock - hours * 3600) / 60;
	int seconds = clock - (minutes * 60 + hours * 3600);
	int miliseconds = (clock - (minutes * 60 + hours * 3600 + seconds)) * 1000.0f;
	std::stringstream stream;
	stream << std::setw(2) << std::setfill('0') << hours << ":" << std::setw(2) << std::setfill('0') << minutes << ":" << std::setw(2) << std::setfill('0') << seconds << ":" << std::setw(3) << std::setfill('0') << miliseconds << std::endl;
	std::string time = stream.str();
	clockText.setString(time);

	sf::Vector2f centerBoxMove = centerBoxPosition - centerBox->getPosition();
	float newX = 0;
	float newY = 0;
	my::vmath::GetUnit(centerBoxMove.x, centerBoxMove.y, &newX, &newY);
	if (newX == newX)
	{
		float moveSpeed = std::min(centerBoxMoveSpeed * deltaTime, my::vmath::GetLength(centerBoxMove.x, centerBoxMove.y));
		centerBoxMove = sf::Vector2f(newX, newY) * moveSpeed;
		centerBox->move(centerBoxMove);
	}
}

void HUD::Draw(DrawManager* drawManager)
{
	drawManager->DrawCall(frameShade, 0.0f);
	drawManager->DrawCall(frame, 0.1f);
	drawManager->DrawCall(centerBox, 0.2f);
	drawManager->DrawCall(textScroll, 0.3f);
	drawManager->DrawCall(reticle, 0.4f);
}

void HUD::SetReticlePosition(float x, float y)
{
	reticle->setPosition(x, y);
}

void HUD::SetReticlePosition(sf::Vector2f position)
{
	reticle->setPosition(position);
}

void HUD::SetCenterBoxPosition(float x, float y)
{
	centerBoxPosition = sf::Vector2f(x, y);
}

void HUD::SetCenterBoxPosition(sf::Vector2f position)
{
	centerBoxPosition = position;
}

sf::View HUD::GetHUDView()
{
	return HUDView;
}

sf::Text HUD::GetClockText()
{
	return clockText;
}

void HUD::DebugUpdate(float deltaTime)
{
	if (deltaTime == 0.0f)
	{
		printf("FPS counter error.");
		return;
	}
	for (int i = 0; i < lastFPSSize - 1; i++)
	{
		lastFPS[i] = lastFPS[i + 1];
	}
	lastFPS[lastFPSSize - 1] = 1.0f / deltaTime;
	float fps = 0;
	for (int i = 0; i < lastFPSSize; i++)
	{
		fps += lastFPS[i];
	}
	fps /= lastFPSSize;
	int iFPS = roundf(fps);
	std::stringstream stream;
	stream << "FPS: " << iFPS;
	debugFPS.setString(stream.str());
}

void HUD::DebugDraw(DrawManager* drawManager)
{
	drawManager->Draw(debugBar);
	drawManager->Draw(&debugFPS);
}

float HUD::GetClock()
{
	return clock;
}
