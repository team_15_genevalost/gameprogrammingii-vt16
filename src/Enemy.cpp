#include "Enemy.hpp"


Enemy::Enemy(AnimatedSprite* walkAnimation, AnimatedSprite* attackAnimation, float zBuffer, sf::Vector2f position, sf::Vector2f origin, float angle) : Entity(walkAnimation, zBuffer, position, angle, origin)
{
	this->walkAnimation = walkAnimation;
	this->attackAnimation = attackAnimation;
	attackAnimation->setPosition(walkAnimation->getPosition());
	attackAnimation->setRotation(walkAnimation->getRotation());
	attackAnimation->setOrigin(walkAnimation->getOrigin());
	moveSpeed = 1200.0f;
	rotationSpeed = 270;
	health = 100;
}

Enemy::Enemy(AnimatedSprite* walkAnimation, AnimatedSprite* attackAnimation, float zBuffer, sf::Vector2f position, Origin origin, float angle) : Entity(walkAnimation, zBuffer, position, origin, angle)
{
	this->walkAnimation = walkAnimation;
	this->attackAnimation = attackAnimation;
	attackAnimation->setPosition(walkAnimation->getPosition());
	attackAnimation->setRotation(walkAnimation->getRotation());
	attackAnimation->setOrigin(walkAnimation->getOrigin());
	moveSpeed = 1200.0f;
	rotationSpeed = 270;
	health = 100;
}

Enemy::~Enemy()
{

}

void Enemy::Update(float deltaTime)
{
	if (sprite == attackAnimation)
	{
		attackAnimation->Update(deltaTime);
		if (attackAnimation->HasEnded())
		{
			walkAnimation->Reset();
			sprite = walkAnimation;
		}
		walkAnimation->setPosition(attackAnimation->getPosition());
		walkAnimation->setRotation(attackAnimation->getRotation());
	}
	else
	{
		if (!(targetPosition == nullptr))
		{
			sf::Vector2f targetHeading = { targetPosition->x - sprite->getPosition().x, targetPosition->y - sprite->getPosition().y };
			my::vmath::Normalize(&targetHeading.x, &targetHeading.y);
			float angle = atan2(targetHeading.y, targetHeading.x) - my::math::DegToRad(sprite->getRotation());
			my::math::Wrap(-M_PI, M_PI, &angle);
			float sign = my::math::Sign(angle);
			if (sign * angle > my::math::DegToRad(rotationSpeed * deltaTime))
			{
				angle = sign * my::math::DegToRad(rotationSpeed * deltaTime);
			}
			Rotate(my::math::RadToDeg(angle));

			float distance = my::vmath::GetLength(sprite->getPosition().x - targetPosition->x, sprite->getPosition().y - targetPosition->y);
			float sine = sin(my::math::DegToRad(GetRotation()));
			float cosine = cos(my::math::DegToRad(GetRotation()));
			sf::Vector2f heading = { cosine, sine };
			float dot = my::vmath::DotProduct(targetHeading.x, targetHeading.y, heading.x, heading.y);

			if (dot < .9f || distance - 200 > moveSpeed * deltaTime)
			{
				walkAnimation->Update(deltaTime);
				Move(cos(my::math::DegToRad(sprite->getRotation())) * moveSpeed * deltaTime, sin(my::math::DegToRad(sprite->getRotation())) * moveSpeed * deltaTime);
			}
			else
			{
				attackAnimation->Reset();
				if (rand() % 2 == 1)
				{
					attackAnimation->scale(1, -1);
				}
				sprite = attackAnimation;
			}
		}
		attackAnimation->setPosition(walkAnimation->getPosition());
		attackAnimation->setRotation(walkAnimation->getRotation());
	}

	if (polygon != nullptr)
	{
		polygon->SetPosition(sprite->getPosition());
		polygon->SetRotation(sprite->getRotation());
	}
}

void Enemy::Damage(float amount)
{
	health -= amount;
}

void Enemy::UpdateMovement()
{

	
}

bool Enemy::IsAlive()
{
	if (health <= 0)
		return false;
	return true;
}

void Enemy::SetTargetPosition(sf::Vector2f* position)
{
	targetPosition = position;
}

void Enemy::SetAttackAnimation(AnimatedSprite* animation)
{
	attackAnimation = animation;
}
