#include "CollisionPolygon.hpp"

CollisionPolygon::CollisionPolygon(sf::Vector2f position, sf::Vector2f origin) : CollisionPrimitive(position, origin)
{
	this->position = position;
	isValid = false;
	isTested = false;
}

sf::FloatRect CollisionPolygon::GetGlobalBounds()
{
	sf::FloatRect rect = GetLocalBounds();
	rect.left += position.x;
	rect.top += position.y;
	return rect;
}

sf::FloatRect CollisionPolygon::GetLocalBounds()
{
	if (vertices.size() == 0)
	{
		return sf::FloatRect();
	}
	float left = vertices[0].x;
	float right = vertices[0].x;
	float top = vertices[0].y;
	float bottom = vertices[0].y;
	for (int i = 1; i < vertices.size(); i++)
	{
		if (vertices[i].x < left)
			left = vertices[i].x;
		if (vertices[i].x > right)
			right = vertices[i].x;
		if (vertices[i].y < top)
			top = vertices[i].y;
		if (vertices[i].y > bottom)
			bottom = vertices[i].y;
	}
	sf::FloatRect rect;
	rect.top = top;
	rect.left = left;
	rect.height = bottom - top;
	rect.width = right - left;
	return rect;
}

sf::Vector2f CollisionPolygon::GetPoint(int n)
{
	return vertices[n];
}

void CollisionPolygon::DeletePoint(int n)
{
	vertices.erase(vertices.begin() + n);
	isTested = false;
}

std::vector<sf::Vector2f> CollisionPolygon::GetPoints()
{
	std::vector<sf::Vector2f> global = vertices;
	for (int i = 0; i < global.size(); i++)
	{
		global[i].x += position.x;
		global[i].y += position.y;
	}
	return global;
}

void CollisionPolygon::AddPoint(sf::Vector2f vertex)
{
	vertices.push_back(vertex);
	isTested = false;
}

void CollisionPolygon::AddPoint(float x, float y)
{
	AddPoint(sf::Vector2f(x, y));
}

int CollisionPolygon::NrOfPoints()
{
	return vertices.size();
}

void CollisionPolygon::GiftWrap()
{
	if (isTested) return;
	std::vector<sf::Vector2f> newVertices;
	
	if (vertices.size() < 3) return;
	sf::Vector2f current = vertices[0];

	for (int i = 1; i < vertices.size(); i++)
	{
		if (vertices[i].x < current.x)
			current = vertices[i];
	}

	do
	{
		newVertices.push_back(current);

		sf::Vector2f next = vertices[0];
		for (int i = 1; i < vertices.size(); i++)
		{
			if (vertices[i] != current)
			{
				float cross = (next.x - current.x)*(vertices[i].y - current.y) - (next.y - current.y)*(vertices[i].x - current.x);
				if (current == next || cross > 0.0f)
				{
					next = vertices[i];
				}
			}
		}
		current = next;
	} while (current != newVertices[0]);

	vertices = newVertices;
	if (vertices.size() >= 3)
		isValid = true;
	else
		isValid = false;
	isTested = true;
}

void CollisionPolygon::Update()
{

}

void CollisionPolygon::FlipHorizontal()
{
	for (int i = 0; i < vertices.size(); i++)
	{
		vertices[i].x = -vertices[i].x;
	}
}

void CollisionPolygon::FlipVertical()
{
	for (int i = 0; i < vertices.size(); i++)
	{
		vertices[i].y = -vertices[i].y;
	}
}

bool CollisionPolygon::IsValid()
{
	if (!isTested)
		GiftWrap();
	return isValid;
}

void CollisionPolygon::RemovePoint(int n)
{
	if (n >= 0 && n < vertices.size())
	{
		vertices.erase(vertices.begin() + n);
	}
}

void CollisionPolygon::MovePoint(int n, sf::Vector2f move)
{
	if (n >= 0 && n < vertices.size())
	{
		vertices[n] += move;
	}
}

void CollisionPolygon::SetPoint(int n, sf::Vector2f point)
{
	if (n >= 0 && n < vertices.size())
	{
		vertices[n] = point;
	}
}

sf::Vector2f CollisionPolygon::GetCenter()
{
	float totalX = 0;
	float totalY = 0;
	for (int i = 0; i < vertices.size(); i++)
	{
		totalX += vertices[i].x;
		totalY += vertices[i].y;
	}
	totalX /= vertices.size();
	totalY /= vertices.size();
	return sf::Vector2f(totalX, totalY);
}

void CollisionPolygon::SetRotation(float rotation)
{
	Rotate(rotation - this->rotation);
}

void CollisionPolygon::Rotate(float rotation)
{
	this->rotation += rotation;
	float angle = my::math::DegToRad(rotation);
	for (int i = 0; i < vertices.size(); i++)
	{
		my::vmath::RotateAroundOrigin(&vertices[i].x, &vertices[i].y, angle);
	}
}

void CollisionPolygon::SetOrigin(sf::Vector2f origin)
{
	float dX = origin.x - this->origin.x;
	float dY = origin.y - this->origin.y;
	for (int i = 0; i < vertices.size(); i++)
	{
		vertices[i].x -= dX;
		vertices[i].y -= dY;
	}
}

void CollisionPolygon::DebugDraw(DrawManager* drawManager, sf::RenderTexture* renderTexture)
{
	sf::ConvexShape shape;
	shape.setPointCount(vertices.size());

	for (int i = 0; i < vertices.size(); i++)
	{
		shape.setPoint(i, vertices[i]);
	}
	shape.setPosition(position);
	shape.setFillColor(sf::Color(0, 0, 0, 0));
	shape.setOutlineColor(sf::Color(255, 255, 0, 128));
	shape.setOutlineThickness(-2.0f);
	drawManager->Draw(&shape, renderTexture);
}
