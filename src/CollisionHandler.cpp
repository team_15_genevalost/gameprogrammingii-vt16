#include "CollisionHandler.hpp"

CollisionHandler::~CollisionHandler()
{
	while (polygons.begin() != polygons.end())
	{
		delete polygons[polygons.size()];
		polygons.pop_back();
	}
}

CollisionPolygon* CollisionHandler::CreatePolygon(sf::Vector2f position)
{
	CollisionPolygon* polygon = new CollisionPolygon(position);
	polygons.push_back(polygon);
	return polygon;
}

CollisionPolygon* CollisionHandler::CreatePolygon(sf::Vector2f position, int numberOfSides, float width, float height)
{
	float angle;
	std::vector<sf::Vector2f> points;
	if (numberOfSides % 2 == 0)
		angle = 0;
	else
		angle = M_PI / 2;
	float step = (M_PI * 2) / numberOfSides;
	CollisionPolygon* polygon = new CollisionPolygon(position);
	for (int i = 0; i < numberOfSides; i++)
	{
		float cosine = cos(angle);
		float sine = sin(angle);
		points.push_back(sf::Vector2f(cosine, sine));
		angle += step;
	}

	float minX, maxX, minY, maxY;
	minX = maxX = points[0].x;
	minY = maxY = points[0].y;
	for (int i = 1; i < points.size(); i++)
	{
		if (points[i].x < minX)
			minX = points[i].x;
		if (points[i].y < minY)
			minY = points[i].y;
		if (points[i].x > maxX)
			maxX = points[i].x;
		if (points[i].y > maxY)
			maxY = points[i].y;
	}

	float scaleX = width / (maxX - minX);
	float scaleY = height / (maxY - minY);

	for (int i = 0; i < points.size(); i++)
	{
		points[i].x *= scaleX;
		points[i].y *= scaleY;
		polygon->AddPoint(points[i]);
	}
	polygon->GiftWrap();
	polygons.push_back(polygon);
	return polygon;
}

CollisionPolygon* CollisionHandler::CreatePolygon(sf::Vector2f position, PolygonShapes shape, float width, float height)
{
	CollisionPolygon* polygon = new CollisionPolygon(position);
	switch (shape)
	{
	case Rectangular:
		
		polygon->AddPoint(width / 2, height / 2);
		polygon->AddPoint(width / 2, -height / 2);
		polygon->AddPoint(-width / 2, height / 2);
		polygon->AddPoint(-width / 2, -height / 2);
		break;
	case RightTriangle:
		polygon->AddPoint(width / 2, height / 2);
		polygon->AddPoint(-width / 2, height / 2);
		polygon->AddPoint(-width / 2, -height / 2);
		break;
	case IsoscelesTriangle:
		polygon->AddPoint(0, -height / 2);
		polygon->AddPoint(width / 2, height / 2);
		polygon->AddPoint(-width / 2, height / 2);	
		break;
	}
	polygon->GiftWrap();
	polygons.push_back(polygon);
	return polygon;
}

void CollisionHandler::AddPolygon(CollisionPolygon* polygon)
{
	polygons.push_back(polygon);
}

bool CollisionHandler::CollisionCheck(CollisionPrimitive* objectA, CollisionPrimitive* objectB)
{
	if (objectA == nullptr || objectB == nullptr)
		return false;
	if (!objectA->GetGlobalBounds().intersects(objectB->GetGlobalBounds()))
		return false;
	if (typeid(CollisionPolygon) == typeid(*objectA) && typeid(CollisionPolygon) == typeid(*objectB))
	{
		return Check(static_cast<CollisionPolygon*>(objectA), static_cast<CollisionPolygon*>(objectB));
	}
}

bool CollisionHandler::Check(CollisionPolygon* objectA, CollisionPolygon* objectB)
{
	sf::Vector2f axis;
	float smallestOverlap = -1.0f;
	int sign = 0;
	sf::Vector2f projection;

	float minA, minB, maxA, maxB;
	minA = minB = maxA = maxB = 0.0f;

	std::vector<sf::Vector2f> shapeA = objectA->GetPoints();
	std::vector<sf::Vector2f> shapeB = objectB->GetPoints();

	for (int i = 0; i < shapeA.size() + shapeB.size(); i++)
	{
		if (i == shapeA.size())
		{
			axis.x = shapeB[shapeB.size() - 1].y - shapeB[0].y;
			axis.y = shapeB[0].x - shapeB[shapeB.size() - 1].x;
		}
		else if (i > shapeA.size())
		{
			axis.x = shapeB[i - shapeA.size() - 1].y - shapeB[i - shapeA.size()].y;
			axis.y = shapeB[i - shapeA.size()].x - shapeB[i - shapeA.size() - 1].x;
		}
		else if (i == 0)
		{
			axis.x = shapeA[shapeA.size() - 1].y - shapeA[0].y;
			axis.y = shapeA[0].x - shapeA[shapeA.size() - 1].x;
		}
		else
		{
			axis.x = shapeA[i - 1].y - shapeA[i].y;
			axis.y = shapeA[i].x - shapeA[i - 1].x;
		}

		my::vmath::Normalize(&axis.x, &axis.y);

		minA = maxA = my::vmath::DotProduct(shapeA[0].x, shapeA[0].y, axis.x, axis.y);

		for (int j = 1; j < shapeA.size(); j++)
		{
			float val = my::vmath::DotProduct(shapeA[j].x, shapeA[j].y, axis.x, axis.y);
			if (val > maxA)
				maxA = val;
			else if (val < minA)
				minA = val;
		}

		minB = maxB = my::vmath::DotProduct(shapeB[0].x, shapeB[0].y, axis.x, axis.y);

		for (int j = 1; j < shapeB.size(); j++)
		{
			float val = my::vmath::DotProduct(shapeB[j].x, shapeB[j].y, axis.x, axis.y);
			if (val > maxB)
				maxB = val;
			else if (val < minB)
				minB = val;
		}

		if (maxA <= minB || minA >= maxB)
			return false;

		float thisSmallest = maxA - minB < maxB - minA ? maxA - minB : maxB - minA;
		if (smallestOverlap <= 0.0f || thisSmallest < smallestOverlap)
		{
			smallestOverlap = maxA - minB < maxB - minA ? maxA - minB : minA - maxB;
			sign = smallestOverlap > 0 ? 1 : -1;
			smallestOverlap *= sign;
			projection = axis;
		}
	}
	smallestOverlap = smallestOverlap > 0.1f ? smallestOverlap : 0.1f;
	projection *= sign * smallestOverlap;

	objectA->SetOnHitProjection(-projection);
	objectB->SetOnHitProjection(projection);
	return true;
}