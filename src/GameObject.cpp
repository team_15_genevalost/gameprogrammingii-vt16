#include "GameObject.hpp"

GameObject::GameObject(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, float angle, sf::Vector2f origin, CollisionPolygon* polygon)
{
	this->sprite = sprite;
	this->zBuffer = zBuffer;
	sprite->setOrigin(origin);
	sprite->setRotation(angle);
	sprite->setPosition(position);
	isVisible = true;
	if (polygon != nullptr)
		polygon->SetOrigin(origin - polygon->GetCenter());
	this->polygon = polygon;
}

GameObject::GameObject(sf::Sprite* sprite, float zBuffer, float x, float y, float angle, float originX, float originY, CollisionPolygon* polygon) : GameObject(sprite, zBuffer, sf::Vector2f(x, y), angle, sf::Vector2f(originX, originY), polygon){}

GameObject::GameObject(sf::Sprite* sprite, float zBuffer, float x, float y, Origin origin, float angle, CollisionPolygon* polygon) : GameObject(sprite, zBuffer, x, y, angle, 0, 0, polygon)
{
	SetOrigin(origin);
}

GameObject::GameObject(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, Origin origin, float angle, CollisionPolygon* polygon) : GameObject(sprite, zBuffer, position.x, position.y, origin, angle, polygon) {}

GameObject::~GameObject()
{
	
}

void GameObject::SetSprite(sf::Sprite* sprite)
{
	this->sprite = sprite;
}

void GameObject::SetPosition(sf::Vector2f position)
{
	sprite->setPosition(position);
	if (polygon != nullptr)
		polygon->SetPosition(position);
}

void GameObject::SetRotation(float angle)
{
	Rotate(angle - sprite->getRotation());
}

sf::Sprite* GameObject::GetSprite()
{
	return sprite;
}

sf::Vector2f GameObject::GetPosition()
{
	return sprite->getPosition();
}

float GameObject::GetRotation()
{
	return sprite->getRotation();
}

void GameObject::Move(sf::Vector2f move)
{
	sprite->move(move);
	if (polygon != nullptr)
		polygon->Move(move);
}

void GameObject::Move(float x, float y)
{
	sf::Vector2f move = sf::Vector2f(x, y);
	Move(move);
}

void GameObject::Rotate(float amount)
{
	sprite->rotate(amount);
	if (polygon != nullptr)
		polygon->Rotate(amount);
}

float GameObject::GetZBuffer()
{
	return zBuffer;
}

void GameObject::SetZBuffer(float zBuffer)
{
	this->zBuffer = zBuffer;
}

sf::Vector2f GameObject::GetOrigin()
{
	return sprite->getOrigin();
}

void GameObject::SetOrigin(sf::Vector2f origin)
{
	sprite->setOrigin(origin);
}

bool GameObject::IsVisible()
{
	return isVisible;
}

void GameObject::SetIsVisible(bool isVisible)
{
	this->isVisible = isVisible;
}

void GameObject::Draw(DrawManager* drawManager)
{
	if(isVisible)
		drawManager->DrawCall(sprite, zBuffer);
}

void GameObject::SetOrigin(Origin origin)
{
	sf::IntRect rect = sprite->getTextureRect();
	switch (origin)
	{
	case Origin::TopLeft:
		sprite->setOrigin(0, 0);
		break;
	case Origin::TopMiddle:
		sprite->setOrigin(rect.width / 2, 0);
		break;
	case Origin::TopRight:
		sprite->setOrigin(rect.width, 0);
		break;
	case Origin::MiddleLeft:
		sprite->setOrigin(0, rect.height / 2);
		break;
	case Origin::Center:
		sprite->setOrigin(rect.width / 2, rect.height / 2);
		break;
	case Origin::MiddleRight:
		sprite->setOrigin(rect.width, rect.height / 2);
		break;
	case Origin::BottomLeft:
		sprite->setOrigin(0, rect.height);
		break;
	case Origin::BottomMiddle:
		sprite->setOrigin(rect.width / 2, rect.height);
		break;
	case Origin::BottomRight:
		sprite->setOrigin(rect.width, rect.height);
		break;
	default:
		break;
	}
}

void GameObject::SetOrigin(float x, float y)
{
	sprite->setOrigin(x, y);
	polygon->SetOrigin(sprite->getOrigin() - polygon->GetCenter());
}

void GameObject::SetCollisionPolygon(CollisionPolygon* polygon)
{
	this->polygon = polygon;
}

CollisionPolygon* GameObject::GetCollisionPolygon()
{
	return polygon;
}
