#include "HighscoreMenuState.hpp"


HighscoreMenuState::HighscoreMenuState()
{

}

HighscoreMenuState::~HighscoreMenuState()
{

}

void HighscoreMenuState::Enter(sf::RenderWindow* window)
{
	font.loadFromFile("..\\resources\\DS-DIGI.TTF");
	this->window = window;
	scaleX = static_cast<float>(WindowSizeX) / static_cast<float>(window->getSize().x);
	scaleY = static_cast<float>(WindowSizeY) / static_cast<float>(window->getSize().y);
	
	view.setSize(WindowSizeX, WindowSizeY);
	view.setCenter(WindowSizeX / 2, WindowSizeY / 2);
	window->setView(view);
	spriteManager = new SpriteManager();

	GUI_Highscore = spriteManager->CreateSprite("..\\resources\\Menuhighscore\\GUI_Highscore.png");
	highscoreBack = spriteManager->CreateSprite("..\\resources\\Menuhighscore\\GUI_HighS_Back.png");
	reticle = spriteManager->CreateSprite("..\\resources\\reticle.png");

	GUI_Highscore->setScale(view.getSize().x / GUI_Highscore->getLocalBounds().width, view.getSize().y / GUI_Highscore->getLocalBounds().height);
	reticle->setOrigin(reticle->getLocalBounds().width / 2, reticle->getLocalBounds().height / 2);
	
	GUI_Highscore->setScale(view.getSize().x / GUI_Highscore->getLocalBounds().width, view.getSize().y / GUI_Highscore->getLocalBounds().height);
	highscoreBack->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	reticle->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	reticle->setOrigin(reticle->getLocalBounds().width / 2, reticle->getLocalBounds().height / 2);

	highscoreBack->setPosition(0.100f * WindowSizeX, 0.8500f * WindowSizeY);

	streamin.open("..\\resources\\level1\\Highscore\\semih.txt");

	if (streamin.is_open())
	{
		streamin >> line[0];
		streamin >> line[1];
		streamin >> line[2];
		streamin >> line[3];
		streamin >> line[4];
		streamin >> line[5];
		streamin >> line[6];
		streamin >> line[7];
		streamin >> line[8];
		streamin >> line[9];
		streamin >> line[10];
		streamin >> line[11];
		streamin >> line[12];
		streamin >> line[13];
		streamin >> line[14];
		streamin >> line[15];
		streamin >> line[16];
		streamin >> line[17];
		streamin >> line[18];
		streamin >> line[19];

		sf::String ScoreName;
		sf::Text Name;
		Name.setFont(font);
		Name.setString(ScoreName);

		HighScore.setFont(font);
		HighScore.setCharacterSize(45);
		HighScore.setString(line[0] +" : "+ line[1]);
		HighScore.setPosition(0.200f * WindowSizeX, 0.2000f * WindowSizeY);

		SecondScore.setFont(font);
		SecondScore.setCharacterSize(45);
		SecondScore.setString(line[2] +" : "+ line[3]);
		SecondScore.setPosition(0.200f * WindowSizeX, 0.3000f * WindowSizeY);

		ThirdScore.setFont(font);
		ThirdScore.setCharacterSize(45);
		ThirdScore.setString(line[4] +" : "+ line[5]);
		ThirdScore.setPosition(0.200f * WindowSizeX, 0.4000f * WindowSizeY);

		ForthScore.setFont(font);
		ForthScore.setCharacterSize(45);
		ForthScore.setString(line[6] +" : "+ line[7]);
		ForthScore.setPosition(0.200f * WindowSizeX, 0.5000f * WindowSizeY);

		FifthScore.setFont(font);
		FifthScore.setCharacterSize(45);
		FifthScore.setString(line[8] + " : " + line[9]);
		FifthScore.setPosition(0.200f * WindowSizeX, 0.6000f * WindowSizeY);

		SixthScore.setFont(font);
		SixthScore.setCharacterSize(45);
		SixthScore.setString(line[10] + " : " + line[11]);
		SixthScore.setPosition(0.6000f * WindowSizeX, 0.2000f * WindowSizeY);

		SeventhScore.setFont(font);
		SeventhScore.setCharacterSize(45);
		SeventhScore.setString(line[12] + " : " + line[13]);
		SeventhScore.setPosition(0.6000f * WindowSizeX, 0.3000f * WindowSizeY);

		EighthScore.setFont(font);
		EighthScore.setCharacterSize(45);
		EighthScore.setString(line[14] + " : " + line[15]);
		EighthScore.setPosition(0.6000f * WindowSizeX, 0.4000f * WindowSizeY);

		NinthScore.setFont(font);
		NinthScore.setCharacterSize(45);
		NinthScore.setString(line[16] + " : " + line[17]);
		NinthScore.setPosition(0.6000f * WindowSizeX, 0.5000f * WindowSizeY);

		TenthScore.setFont(font);
		TenthScore.setCharacterSize(45);
		TenthScore.setString(line[18] + " : " + line[19]);
		TenthScore.setPosition(0.6000f * WindowSizeX, 0.6000f * WindowSizeY);

		streamin.close();

	}

	mouse = sf::Mouse();
	window->setMouseCursorVisible(false);
	mousePosition = new sf::Vector2f();
	drawManager = new DrawManager(window);
}

bool HighscoreMenuState::Update(float deltaTime)
{
	RECT rect = RECT();
	POINT point = POINT();
	sf::WindowHandle windowHandle = window->getSystemHandle();
	GetClientRect(windowHandle, &rect);
	ClientToScreen(windowHandle, &point);

	reticle->setPosition(sf::Vector2f((mouse.getPosition(*window).x) * scaleX, (mouse.getPosition(*window).y) * scaleY));

	if (highscoreBack->getGlobalBounds().contains(reticle->getPosition()))
	{
		highscoreBack->setColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			return false;
		}
	}
	else
	{
		highscoreBack->setColor(sf::Color::White);
	}

	return true;
}

void HighscoreMenuState::Exit()
{
}

void HighscoreMenuState::Draw()
{
	window->clear(sf::Color::Blue);
	window->draw(*GUI_Highscore);
	window->draw(*highscoreBack);
	window->draw(HighScore);
	window->draw(SecondScore);
	window->draw(ThirdScore);
	window->draw(ForthScore);
	window->draw(FifthScore);
	window->draw(SixthScore);
	window->draw(SeventhScore);
	window->draw(EighthScore);
	window->draw(NinthScore);
	window->draw(TenthScore);
	window->draw(*reticle);
	window->display();
}

IState* HighscoreMenuState::NextState()
{
	state = new MenuState();

	return state;
}
