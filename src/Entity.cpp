#include "Entity.hpp"

Entity::Entity(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, float angle, sf::Vector2f origin) : GameObject(sprite, zBuffer, position, angle, origin) {}

Entity::Entity(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, Origin origin, float angle) : GameObject(sprite, zBuffer, position.x, position.y, origin, angle) {}

Entity::Entity(sf::Sprite* sprite, float zBuffer, float x, float y, float angle, float originX, float originY) : GameObject(sprite, zBuffer, x, y, angle, originX, originY) {}

Entity::Entity(sf::Sprite* sprite, float zBuffer, float x, float y, Origin origin, float angle) : GameObject(sprite, zBuffer, x, y, origin, angle) {}


void Entity::Update(float deltaTime)
{
	if (typeid(AnimatedSprite) == typeid(*sprite))
	{
		static_cast<AnimatedSprite*>(sprite)->Update(deltaTime);
	}
}

Entity::~Entity()
{
}