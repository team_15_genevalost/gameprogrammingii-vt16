#include "AnimatedSprite.hpp"

AnimatedSprite::AnimatedSprite(const sf::Texture & texture, int xCount, int yCount, AnimationType animType)
{
	setTexture(texture);
	sf::IntRect rect(0, 0, texture.getSize().x / xCount, texture.getSize().y / yCount);
	if (animType == ReverseRunOnce || animType == ReverseLooping)
	{
		rect.left = (xCount - 1) * rect.width;
		rect.top = (yCount - 1) * rect.height;
	}
	updateFrequency = 50;
	accumulator = 0.0f;
	this->animType = animType;
	hasEnded = false;

	setTextureRect(rect);
}

void AnimatedSprite::SetUpdateFrequency(float val)
{
	updateFrequency = val;
}

float AnimatedSprite::GetUpdateFrequency()
{
	return updateFrequency;
}

void AnimatedSprite::Update(float deltaTime)
{
	accumulator += deltaTime;
	if (updateFrequency == 0)
		NextSprite();
	while (accumulator >= 1 / updateFrequency)
	{
		if ((hasEnded && animType == BackAndForth) || animType == ReverseLooping || animType == ReverseRunOnce)
			PreviousSprite();
		else if (animType == Random)
			RandomSprite();
		else
			NextSprite();
		accumulator -= 1 / updateFrequency;
	}
	while (accumulator <= -(1 / updateFrequency))
	{
		if ((hasEnded && animType == BackAndForth) || animType == ReverseLooping || animType == ReverseRunOnce)
			NextSprite();
		else if (animType == Random)
			RandomSprite();
		else
			PreviousSprite();
		accumulator += 1 / updateFrequency;
	}
}

int AnimatedSprite::CurrentFrame()
{
	int xCount = getTexture()->getSize().x / getTextureRect().width;
	return getTextureRect().left / getTextureRect().width + (getTextureRect().top / getTextureRect().height) * xCount;
}

bool AnimatedSprite::HasEnded()
{
	return hasEnded;
}

void AnimatedSprite::Reset()
{ 
	if (animType == ReverseRunOnce || animType == ReverseLooping)
		SetSprite(GetNrOfFrames() - 1);
	else
		SetSprite(0);
	hasEnded = false;
	accumulator = 0;
}

void AnimatedSprite::SetSprite(int n)
{
	if (n >= GetNrOfFrames())
		return;

	int xCount = getTexture()->getSize().x / getTextureRect().width;
	
	int x = n % xCount;
	int y = n / xCount;

	sf::IntRect rect = getTextureRect();

	rect.left = x * rect.width;
	rect.top = y * rect.height;

	setTextureRect(rect);
}

int AnimatedSprite::GetNrOfFrames()
{
	int xCount = getTexture()->getSize().x / getTextureRect().width;
	int yCount = getTexture()->getSize().y / getTextureRect().height;
	return xCount * yCount;
}

AnimatedSprite::~AnimatedSprite()
{
}

void AnimatedSprite::NextSprite()
{
	sf::IntRect rect = getTextureRect();
	if (!(rect.left + rect.width >= getTexture()->getSize().x))
	{
		rect.left += rect.width;
	}
	else if (!(rect.top + rect.height >= getTexture()->getSize().y))
	{
		rect.left = 0;
		rect.top += rect.height;
	}
	else
	{
		switch (animType)
		{
		case Looping:
		case ReverseLooping:
			rect.left = 0;
			rect.top = 0;
			break;
		case BackAndForth:
			hasEnded = !hasEnded;
			PreviousSprite();
			return;
		case RunOnce:
		case ReverseRunOnce:
			hasEnded = true;
			return;
		default:
			break;
		}
	}
	
	setTextureRect(rect);
}

void AnimatedSprite::PreviousSprite()
{
	sf::IntRect rect = getTextureRect();
	if (rect.left - rect.width >= 0)
	{
		rect.left -= rect.width;
	}
	else if (rect.top - rect.height >= 0)
	{
		rect.left = getTexture()->getSize().x - rect.width;
		rect.top -= rect.height;
	}
	else
	{
		switch (animType)
		{
		case Looping:
		case ReverseLooping:
			rect.left = getTexture()->getSize().x - rect.width;
			rect.top = getTexture()->getSize().y - rect.height;
			break;
		case BackAndForth:
			hasEnded = !hasEnded;
			NextSprite();
			return;
		case RunOnce:
		case ReverseRunOnce:
			hasEnded = true;
			return;
		default:
			break;
		}
	}
	setTextureRect(rect);
}

void AnimatedSprite::RandomSprite()
{
	int random = rand() % GetNrOfFrames();
	SetSprite(random);
}
