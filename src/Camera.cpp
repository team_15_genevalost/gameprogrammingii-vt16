#include "Camera.hpp"

Camera::Camera(sf::Vector2f position, float width, float height, sf::Vector2f* cameraFocus, float focus)
{
	this->position = position;
	this->width = width;
	this->height = height;
	this->cameraFocus = cameraFocus;
	this->focus = focus;
	cameraOffset = { 0, 0 };
	zoom = 1.0f;
	cameraView = sf::View(GetCameraRect());
}

void Camera::Update(sf::Vector2f position)
{
	if (cameraFocus == nullptr || my::vmath::GetLength(cameraFocus->x - GetCenter().x, cameraFocus->y - GetCenter().y) == 0.0f)
	{
		this->position = position;
	}
	else
	{
		float xMod = my::math::Lerp(0, (*cameraFocus - GetCenter()).x * focus * height / width, pow((cameraFocus->x - GetCenter().x) / (width / 2), 2));
		float yMod = my::math::Lerp(0, (*cameraFocus - GetCenter()).y * focus * width / height, pow((cameraFocus->y - GetCenter().y) / (height / 2), 2));

		this->position = position + sf::Vector2f(xMod, yMod);	
	}

	cameraOffset *= 0.8f;
	if (abs(cameraOffset.x) <= 1)
		cameraOffset.x = 0;
	if (abs(cameraOffset.y) <= 1)
		cameraOffset.y = 0;

	cameraView.setSize(width * zoom, height * zoom);
	cameraView.setCenter(this->position + cameraOffset);
}

sf::Vector2f Camera::GetCenter()
{
	return sf::Vector2f(width / 2, height / 2);
}

sf::Vector2f Camera::GetPosition()
{
	return cameraView.getCenter();
}

sf::FloatRect Camera::GetCameraRect()
{
	sf::FloatRect cameraRect;
	cameraRect.height = height * zoom;
	cameraRect.width = width * zoom;
	cameraRect.left = cameraView.getCenter().x - (width * zoom) / 2;
	cameraRect.top = cameraView.getCenter().y - (height * zoom) / 2;
	return cameraRect;
}

void Camera::AddOffset(sf::Vector2f offset)
{
	cameraOffset += offset;
}

void Camera::AddOffset(float x, float y)
{
	AddOffset(sf::Vector2f(x, y));
}

sf::View Camera::GetCameraView()
{
	return cameraView;
}

void Camera::Zoom(float amount)
{
	zoom += amount;
	if (zoom < 0.2f)
		zoom = 0.2f;
}

void Camera::SetZoom(float zoom)
{
	this->zoom = zoom;
	if (zoom < 0.2f)
		zoom = 0.2f;
}

float Camera::GetZoom()
{
	return zoom;
}

Camera::~Camera()
{

}