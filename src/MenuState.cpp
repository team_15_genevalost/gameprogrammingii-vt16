#include "MenuState.hpp"

MenuState::MenuState()
{
}

MenuState::~MenuState()
{
	delete spriteManager;
	delete drawManager;
	delete menuGloben;
	delete menuLogo;
	delete menuStart;
	delete menuOptions;
	delete highscoreMenuButton;
}

void MenuState::Enter(sf::RenderWindow* window)
{
	isHighscoreButtonPressed = false;

	this->window = window;
	scaleX = static_cast<float>(WindowSizeX) / static_cast<float>(window->getSize().x);
	scaleY = static_cast<float>(WindowSizeY) / static_cast<float>(window->getSize().y);

	view.setSize(WindowSizeX, WindowSizeY);
	view.setCenter(WindowSizeX / 2, WindowSizeY / 2);
	window->setView(view);
	spriteManager = new SpriteManager();
	
	menuGloben = spriteManager->CreateSprite("..\\resources\\menuGloben.png");
	menuLogo = spriteManager->CreateSprite("..\\resources\\menuLogo2.png");
	menuStart = spriteManager->CreateSprite("..\\resources\\menuStart.png");
	menuOptions = spriteManager->CreateSprite("..\\resources\\menuOptions.png");
	highscoreMenuButton = spriteManager->CreateSprite("..\\resources\\Menuhighscore\\highscoreMenuButton.png");
	menuExit = spriteManager->CreateSprite("..\\resources\\menuExit.png");
	reticle = spriteManager->CreateSprite("..\\resources\\reticle.png");

	menuGloben->setScale(view.getSize().x / menuGloben->getLocalBounds().width, view.getSize().y / menuGloben->getLocalBounds().height);
	menuLogo->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	menuStart->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	menuOptions->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	menuExit->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	highscoreMenuButton->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	reticle->setScale(WindowSizeX / 1920.0f, WindowSizeY / 1080.0f);
	reticle->setOrigin(reticle->getLocalBounds().width / 2, reticle->getLocalBounds().height / 2);

	menuLogo->setPosition(0.3645f * WindowSizeX, 0.324 * WindowSizeY);
	menuStart->setPosition(0.6302f * WindowSizeX,0.5755f * WindowSizeY);
	menuOptions->setPosition(0.677f * WindowSizeX, 0.6481f * WindowSizeY);
	highscoreMenuButton->setPosition(0.684f * WindowSizeX, 0.7200f * WindowSizeY);
	menuExit->setPosition(0.6567f * WindowSizeX, 0.7907f * WindowSizeY);

	mouse = sf::Mouse();
	window->setMouseCursorVisible(false);
	mousePosition = new sf::Vector2f();
	drawManager = new DrawManager(window);	
}

bool MenuState::Update(float deltaTime)
{
	RECT rect = RECT();
	POINT point = POINT();
	sf::WindowHandle windowHandle = window->getSystemHandle();
	GetClientRect(windowHandle, &rect);
	ClientToScreen(windowHandle, &point);

	reticle->setPosition(sf::Vector2f((mouse.getPosition(*window).x) * scaleX, (mouse.getPosition(*window).y) * scaleY));
	
	if (menuStart->getGlobalBounds().contains(reticle->getPosition()))
	{
		menuStart->setColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			return false;
		}
	}
	else
	{
		menuStart->setColor(sf::Color::White);
	}
	
	if (menuOptions->getGlobalBounds().contains(reticle->getPosition()))
	{
		menuOptions->setColor(sf::Color::Red);
	}
	else
	{
		menuOptions->setColor(sf::Color::White);
	}	

	if (highscoreMenuButton->getGlobalBounds().contains(reticle->getPosition()))
	{
		highscoreMenuButton->setColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			isHighscoreButtonPressed = true;
			return false;
		}
	}
	else
	{
		highscoreMenuButton->setColor(sf::Color::White);
	}
	
	if (menuExit->getGlobalBounds().contains(reticle->getPosition()))
	{
		menuExit->setColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			window->close();
		}
	}
	else
	{
		menuExit->setColor(sf::Color::White);
	}

	return true;
}

void MenuState::Exit()
{
}

void MenuState::Draw()
{	
	window->clear(sf::Color::Blue);
	window->draw(*menuGloben);
	window->draw(*menuLogo);
	window->draw(*menuStart);
	window->draw(*menuOptions);
	window->draw(*highscoreMenuButton);
	window->draw(*menuExit);
	window->draw(*reticle);
	window->display();
}

IState* MenuState::NextState()
{
	if (isHighscoreButtonPressed == true)
	{
		state = new HighscoreMenuState();
	}
	else
	{
		state = new GameState();
	}
	return state;
}
