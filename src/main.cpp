// main.cpp
#pragma once

#include "Projectile.hpp"
#include "Include.hpp"
#include "SpriteManager.hpp"
#include "AnimatedSprite.hpp"
#include "Camera.hpp"
#include "DrawManager.hpp"
#include "Entity.hpp"
#include "Player.hpp"
#include "Enemy.hpp"
#include "MenuState.hpp"
#include "GameState.hpp"
#include <time.h>
#include <iostream>
#include <Windows.h>
#include "SFML\OpenGL.hpp"

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#pragma comment(lib, "sfml-audio.lib")
#endif

int main(int argc, char** argv)
{
	sf::Clock clock;
	sf::Clock deltaTime;
	sf::RenderWindow* window = new sf::RenderWindow();
	window->create(sf::VideoMode(1280, 720), "OpenGL", sf::Style::Close | sf::Style::Titlebar);
	glEnable(GL_TEXTURE_2D);
	if (!window->isOpen()) return -1;

	float targetTime = 1.0f / 60.0f;
	float accumulator = 0.0f;
	float frameTime = 0.0f;

	IState* state;
	state = new MenuState();
	state->Enter(window);
	
	while (window->isOpen())
	{
		sf::Time time = clock.restart();
		frameTime = std::min(time.asSeconds(), 0.1f);
		accumulator += frameTime;
		while (accumulator > targetTime)
		{
			accumulator -= targetTime;

			sf::Event event;
			while (window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					window->close();
			}
			if (!state->Update(deltaTime.getElapsedTime().asSeconds()))
			{
				IState* tempState = state;
				state = state->NextState();
				state->Enter(window);
				delete tempState;
			}
			deltaTime.restart();
		}
		state->Draw();
	}
	return 0;
}