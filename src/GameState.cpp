#include "GameState.hpp"


GameState::GameState()
{
}

GameState::~GameState()
{
	delete player;
	delete drawManager;
	delete camera;
	delete spriteManager;
	delete hud;
}

void GameState::Enter(sf::RenderWindow* window)
{
	srand(static_cast<int>(GetCurrentTime()));

	sf::Font font;
	this->window = window;

	scaleX = static_cast<float>(WindowSizeX) / static_cast<float>(window->getSize().x);
	scaleY = static_cast<float>(WindowSizeY) / static_cast<float>(window->getSize().y);

	timeManip = 2.0f;

	minPitch = 10;
	maxPitch = 10;
	randomPitch;
	currentPitch;

	lastRightClick = false;
	lastLeftClick = false;
	isBloodVisible = false;
	isPause = false;
	isDebug = false;
	previousEscapeState = false;
	backToMenu = false;

	spriteManager = new SpriteManager();

	grayShader.loadFromFile("..\\resources\\grayscale.glsl", sf::Shader::Fragment);
	grayShader.setParameter("resolution", sf::Vector2f(window->getSize().x, window->getSize().y));

	buzzShader.loadFromFile("..\\resources\\buzz.glsl", sf::Shader::Fragment);
	buzzShader.setParameter("resolution", sf::Vector2f(window->getSize().x, window->getSize().y));

	fireShader.loadFromFile("..\\resources\\fire.glsl", sf::Shader::Fragment);
	fireShader.setParameter("resolution", sf::Vector2f(window->getSize().x, window->getSize().y));

	death = spriteManager->CreateSprite("..\\resources\\death_sprite.png");
	background = spriteManager->CreateSprite("..\\resources\\level1\\BG_tile_plattan.png");

	pause = spriteManager->CreateSprite("..\\resources\\pause screen.png");
	pauseOptions = spriteManager->CreateSprite("..\\resources\\pauseOptions.png");

	player = new Player(spriteManager->CreateSprite("..\\resources\\level1\\player\\player_walk_torso.png", 8, 1, 20.0f),
		spriteManager->CreateSprite("..\\resources\\level1\\player\\player_walk_leg.png", 8, 1, 20.0f),
		spriteManager->CreateSprite("..\\resources\\level1\\player\\player_walk_light.png", 8, 1, 20.0f),
		spriteManager->CreateSprite("..\\resources\\level1\\player\\playerShoot\\player_shoot_torso.png", 6, 1, 20.0f, RunOnce),
		spriteManager->CreateSprite("..\\resources\\level1\\player\\playerShoot\\player_shoot_light.png", 6, 1, 20.0f, RunOnce), 1.0f, sf::Vector2f(0, 0), 0, sf::Vector2f(121, 112));
	player->SetPosition(sf::Vector2f(5000, 4000));

	float width = player->GetSprite()->getGlobalBounds().width;
	float height = player->GetSprite()->getGlobalBounds().height;

	CollisionPolygon* playerPolygon = collisionHandler.CreatePolygon(player->GetPosition(), 11, height * .75f, height * .75f);
	for (int i = 0; i < playerPolygon->NrOfPoints(); i++)
	{
		playerPolygon->MovePoint(i, sf::Vector2f(-width * 0.175f, 0));
	}
	playerPolygon->GiftWrap();
	playerPolygon->SetOrigin(-sf::Vector2f(width / 2, height / 2) + player->GetOrigin());

	player->SetCollisionPolygon(playerPolygon);

	for (int i = 0; i < 10; i++)
	{
		int x = rand() % 10000 - 1000;
		int y = rand() % 10000 - 1000;

		Enemy* enemyTest = new Enemy(spriteManager->CreateSprite("..\\resources\\level1\\crawler\\crawlerwalk_ss.png", 9, 1, 20.0f),
			spriteManager->CreateSprite("..\\resources\\level1\\crawler\\crawlerswipe_ss.png", 12, 1, 40.0f, RunOnce),
			2.0f, sf::Vector2f(x, y), Center);
		enemyTest->SetCollisionPolygon(collisionHandler.CreatePolygon(enemyTest->GetPosition(),
			6, enemyTest->GetSprite()->getLocalBounds().width * 0.5f,
			enemyTest->GetSprite()->getLocalBounds().height * 0.35f));
		enemyTest->SetTargetPosition(&(player->GetPosition()));

		enemies.push_back(enemyTest);
	}

	background2 = spriteManager->CreateSprite("..\\resources\\level1\\LEVEL_1_env.png", 0, 0, 256, 256);

	death->setOrigin(death->getLocalBounds().width / 2, death->getLocalBounds().height / 2);
	pause->setOrigin(pause->getLocalBounds().width / 2, pause->getGlobalBounds().height / 2);
	pauseOptions->setPosition(580, 690);

	mouse = sf::Mouse();
	window->setMouseCursorVisible(false);

	mousePosition = new sf::Vector2f();
	camera = new Camera(player->GetPosition(), WindowSizeX, WindowSizeY, mousePosition, 0.2f);
	camera->SetZoom(1.8f);
	drawManager = new DrawManager(window);

	pause->setPosition(sf::Vector2f(camera->GetCameraView().getSize()) * 0.5f);
	hud = new HUD(spriteManager, WindowSizeX, WindowSizeY);

	buffer.loadFromFile("..\\resources\\level1\\player\\rifle_single_shot.wav");
	rifleSound.setBuffer(buffer);

	buffer2.loadFromFile("..\\resources\\SFX - Flamethrower.wav");
	flameThrower.setBuffer(buffer2);

	buffer3.loadFromFile("..\\resources\\male grunt sound.wav");
	maleGruntSound.setBuffer(buffer3);

	oScore1.LoadHighscores();

	oScore1.add("PDL", 770);
	oScore1.add("MLY", 999);
	oScore1.add("MAC", 452);
	oScore1.add("SIS", 3);

	Populate();
}

bool GameState::Update(float deltaTime)
{
	srand(static_cast<int>(GetCurrentTime()));
	float gameTime = deltaTime * timeManip;

	RECT rect = RECT();
	POINT point = POINT();
	sf::WindowHandle windowHandle = window->getSystemHandle();
	GetClientRect(windowHandle, &rect);
	ClientToScreen(windowHandle, &point);


	rect.left += point.x;
	rect.right += point.x;
	rect.top += point.y;
	rect.bottom += point.y;

	if (!window->hasFocus())
		isPause = true;

	if (sf::Keyboard().isKeyPressed(sf::Keyboard::Escape) && !previousEscapeState)
	{
		isPause = !isPause;
		sf::Mouse::setPosition(sf::Vector2i(mousePosition->x / scaleX, mousePosition->y / scaleY), *window);
	}

	if (sf::Keyboard().isKeyPressed(sf::Keyboard::F3) && !previousF3State)
	{
		isDebug = !isDebug;
	}

	previousF3State = sf::Keyboard().isKeyPressed(sf::Keyboard::F3);
	previousEscapeState = sf::Keyboard().isKeyPressed(sf::Keyboard::Escape);
	if (isPause)
	{
		window->setMouseCursorVisible(true);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			backToMenu = true;
			return false;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Delete))
		{
			window->close();
		}

		ClipCursor(NULL);
		return true;
	}
	else
		window->setMouseCursorVisible(false);

	ClipCursor(&rect);

	for (int i = 0; i < enemies.size(); i++)
	{
		if (!enemies[i]->IsAlive())
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\death_sprite2.png");
			sprite->setColor(sf::Color(255, 255, 255, 200));
			sprite->setScale(1.5f, 1.5f);
			GameObject deathTrail = GameObject(sprite, 0.5, enemies[i]->GetPosition().x, enemies[i]->GetPosition().y, Origin::Center, rand() % 360);
			decoLayer.push_back(deathTrail);
			delete enemies[i];
			enemies.erase(enemies.begin() + i);
			i--;
		}
		else
		{
			enemies[i]->SetTargetPosition(&player->GetPosition());

			enemies[i]->Update(gameTime);
			for (int j = i; j < enemies.size(); j++)
			{
				if (i != j)
				{
					while (CollisionHandler::CollisionCheck(enemies[i]->GetCollisionPolygon(), enemies[j]->GetCollisionPolygon()))
					{
						enemies[i]->Move(enemies[i]->GetCollisionPolygon()->GetOnHitProjection());
					}
				}
			}
		}
	}

	if (player->RemainingHealth() > 0)
	{
		*mousePosition = sf::Vector2f(mouse.getPosition(*window).x * scaleX, mouse.getPosition(*window).y * scaleY);
		sf::Vector2f relative = (*mousePosition - camera->GetCenter()) - (player->GetPosition() - camera->GetPosition()) * (1 / camera->GetZoom());
		float angle = atan2(relative.y, relative.x);

		player->SetRotation(my::math::RadToDeg(angle));

		hud->SetReticlePosition(sf::Vector2f(*mousePosition));
		hud->Update(gameTime);

		player->Update(gameTime);

		for (int i = 0; i < enemies.size(); i++)
		{
			if (my::vmath::Distance(player->GetPosition().x, player->GetPosition().y, enemies[i]->GetPosition().x, enemies[i]->GetPosition().y) < 225)
			{

				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\death_sprite2.png");
				sprite->setColor(sf::Color(255, 255, 255, 200));
				sprite->setScale(.75f, .75f);
				GameObject deathTrail = GameObject(sprite, 0.5, player->GetPosition().x, player->GetPosition().y, Origin::Center, rand() % 360);
				decoLayer.push_back(deathTrail);

				player->Damage(crawlerDamage);

				float currentPitch2 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
				float pitchMin = 0.9f;
				float pitchMax = 1.1f;

				currentPitch2 = currentPitch2 * (pitchMax - pitchMin) + pitchMin;
				maleGruntSound.setPitch(currentPitch2);
				maleGruntSound.play();
			}
			while (CollisionHandler::CollisionCheck(enemies[i]->GetCollisionPolygon(), player->GetCollisionPolygon()))
			{
				bool checkCollish = false;
				enemies[i]->Move(enemies[i]->GetCollisionPolygon()->GetOnHitProjection() * 0.9f);
				player->Move(player->GetCollisionPolygon()->GetOnHitProjection() * 0.1f);
				player->GetCollisionPolygon()->SetPosition(player->GetPosition());
			}
		}

		bool isCollide;
		int count = 0;
		do
		{
			isCollide = false;
			sf::Vector2f move;
			for (int i = 0; i < levelObjects.size(); i++)
			{
				if (CollisionHandler::CollisionCheck(player->GetCollisionPolygon(), levelObjects[i]->GetCollisionPolygon()))
				{
					move += player->GetCollisionPolygon()->GetOnHitProjection();
					isCollide = true;
				}
			}
			player->Move(move);
			if (count > 2)
			{
				player->SetPosition(player->GetPreviousPosition());
				break;
			}
			count++;
		} while (isCollide);
		for (int i = 0; i < player->GetProjectileQueue(); i++)
		{
			float angle = my::math::DegToRad(player->GetRotation());
			float sine = sin(angle);
			float cosine = cos(angle);
			float x = player->GetPosition().x * cosine - player->GetPosition().y * sine;
			float y = player->GetPosition().x * sine + player->GetPosition().y * cosine;
			Projectile projectile = Projectile(spriteManager->CreateSprite("..\\resources\\ALEX_LASER.png"), 5.0f, player->GetPosition().x, player->GetPosition().y, Origin::MiddleLeft);
			projectile.SetCollisionPolygon(collisionHandler.CreatePolygon(projectile.GetPosition(), Rectangular, projectile.GetSprite()->getGlobalBounds().width, projectile.GetSprite()->getGlobalBounds().height));
			projectile.SetRotation(player->GetRotation());
			projectilesPlayer.push_back(projectile);
			camera->AddOffset(cosine * 50, sine * 50);
			camera->AddOffset(rand() % 10 - 5, rand() % 10 - 5);

			currentPitch = rand() % maxPitch + minPitch;

			rifleSound.setPitch((currentPitch) / 10);

			rifleSound.play();
		}

		int queue = player->GetFirePowerQueue();
		for (int i = 0; i < queue; i++)
		{
			bool flame = false;
			float cosine;
			float sine;

			sf::Vector2f scale = { 5.0f, 5.0f };
			sf::Vector2f polyScale = { 0.6f, 0.6f };

			flame = true;
			float angle = my::math::DegToRad(player->GetRotation());
			sine = sin(angle);
			cosine = cos(angle);
			float x = player->GetPosition().x * cosine - player->GetPosition().y * sine;
			float y = player->GetPosition().x * sine + player->GetPosition().y * cosine;
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\fireStencil.png");
			sprite->setScale(scale);
			float speed = my::math::fRand(700.0f, 1050.0f);
			Projectile projectile = Projectile(sprite, 6.0f, player->GetPosition().x, player->GetPosition().y, Origin::MiddleLeft, 0, 1.5f, speed, 0.75f);
			sf::Vector2f center = { projectile.GetSprite()->getGlobalBounds().width * 0.5f, projectile.GetSprite()->getGlobalBounds().height * 0.5f};
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(projectile.GetSprite()->getPosition(), 5, center.x * polyScale.x, center.y * polyScale.y);
			sf::Vector2f relativeOrigin = sf::Vector2f(projectile.GetOrigin().x * scale.x, projectile.GetOrigin().y * scale.y) -  center;
			for (int j = 0; j < polygon->NrOfPoints(); j++)
			{
				polygon->MovePoint(j, -sf::Vector2f(relativeOrigin.x * polyScale.x, relativeOrigin.y * polyScale.y));
			}
			projectile.SetCollisionPolygon(polygon);
			projectile.SetRotation(player->GetRotation() + my::math::fRand(-15.0f, 15.0f));

			projectilesFire.push_back(projectile);

			camera->AddOffset(cosine * 15, sine * 15);
			camera->AddOffset(rand() % 10 - 5, rand() % 10 - 5);
		}
	}
	else
	{
		player->SetIsVisible(false);
	}

	for (int i = 0; i < projectilesPlayer.size(); i++)
	{
		bool isDestroyed = false;
		projectilesPlayer[i].Update(gameTime);
		if (projectilesPlayer[i].IsDestroyed())
		{
			isDestroyed = true;
		}
		for (int j = 0; j < enemies.size(); j++)
		{
			if (CollisionHandler::CollisionCheck(enemies[j]->GetCollisionPolygon(), projectilesPlayer[i].GetCollisionPolygon()))
			{
				enemies[j]->Damage(projectilesPlayer[i].GetDamage());
				isDestroyed = true;
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\death_sprite2.png");
				sprite->setColor(sf::Color(255, 255, 255, 200));
				sprite->setScale(.75f, .75f);
				GameObject deathTrail = GameObject(sprite, 0.5, projectilesPlayer[i].GetPosition().x, projectilesPlayer[i].GetPosition().y, Origin::Center, rand() % 360);
				decoLayer.push_back(deathTrail);
				break;
			}
		}
		for (int j = 0; j < levelObjects.size(); j++)
		{
			if (CollisionHandler::CollisionCheck(levelObjects[j]->GetCollisionPolygon(), projectilesPlayer[i].GetCollisionPolygon()))
			{
				isDestroyed = true;
				break;
			}
		}
		if (isDestroyed)
		{
			projectilesPlayer.erase(projectilesPlayer.begin() + i);
			i--;
		}
	}

	for (int i = 0; i < projectilesFire.size(); i++)
	{
		bool isDestroyed = false;
		projectilesFire[i].Update(gameTime);
		if (projectilesFire[i].IsDestroyed())
		{
			isDestroyed = true;
		}
		for (int j = 0; j < enemies.size(); j++)
		{
			if (CollisionHandler::CollisionCheck(enemies[j]->GetCollisionPolygon(), projectilesFire[i].GetCollisionPolygon()))
			{
				enemies[j]->Damage(projectilesFire[i].GetDamage());
			}
		}
		for (int j = 0; j < levelObjects.size(); j++)
		{
			if (CollisionHandler::CollisionCheck(levelObjects[j]->GetCollisionPolygon(), projectilesFire[i].GetCollisionPolygon()))
			{
				isDestroyed = true;
				break;
			}
		}
		if (isDestroyed)
		{
			projectilesFire.erase(projectilesFire.begin() + i);
			i--;
		}
	}

	lastLeftClick = mouse.isButtonPressed(mouse.Left);
	lastRightClick = mouse.isButtonPressed(mouse.Right);

	camera->Update(player->GetPosition());
	hud->SetCenterBoxPosition((player->GetPosition() - camera->GetPosition()) * (1.0f / camera->GetZoom()) + hud->GetHUDView().getSize() * 0.5f);

	hud->DebugUpdate(deltaTime);

	timeManip = my::math::Lerp(0.4f, 1.0f, std::min(player->RemainingHealth(), 50.0f) / 50.0f);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
	{
		return false;
	}

	timeManip = my::math::Lerp(0.4f, 1.0f, std::min(player->RemainingHealth(), 50.0f) / 50.0f);
	return true;
}

void GameState::Exit()
{
}

void GameState::Draw()
{
	sf::RenderTexture fireTex;
	fireTex.create(window->getSize().x, window->getSize().y);
	fireTex.setView(camera->GetCameraView());
	fireTex.clear(sf::Color::Black);

	for (int i = 0; i < projectilesFire.size(); i++)
	{
		projectilesFire[i].Draw(drawManager);
	}

	drawManager->Present(sf::Color::White, &fireTex);
	drawManager->ClearBuffer();
	fireTex.display();
	sf::Sprite fire(fireTex.getTexture());

	fireShader.setParameter("backbuffer", *fire.getTexture());
	fireShader.setParameter("time", hud->GetClock());
	
	sf::RenderTexture rTex;
	rTex.create(window->getSize().x, window->getSize().y);
	rTex.setView(camera->GetCameraView());
	rTex.clear();
	window->clear(sf::Color(0x11, 0x22, 0x33, 0xff));

	player->Draw(drawManager);

	if (isBloodVisible)
	{
		drawManager->DrawCall(death, 0.1f);
	}
	for (int i = 0; i < decoLayer.size(); i++)
	{
		decoLayer[i].Draw(drawManager);
	}

	drawManager->DrawCall(background, 0.0f);

	for (int i = 0; i < enemies.size(); i++)
	{
		enemies[i]->Draw(drawManager);
	}

	for (int i = 0; i < decoLayer.size(); i++)
	{
		decoLayer[i].Draw(drawManager);
	}

	for (int i = 0; i < projectilesPlayer.size(); i++)
	{
		projectilesPlayer[i].Draw(drawManager);
	}

	int backgroundWidth = background->getLocalBounds().width;
	int backgroundHeight = background->getLocalBounds().height;
	for (int x = 0; x < 11; x++)
	{
		for (int y = 0; y < 12; y++)
		{
			background->setPosition(-y * 191 + x * backgroundWidth, y * backgroundHeight);
			drawManager->Draw(background, &rTex);
		}
	}
	
	for (int i = 0; i < backdrop.size(); i++)
	{
		float zBuffer = i >= 20 ? 0.1f : 10.0f;
		drawManager->DrawCall(backdrop[i], zBuffer);
	}

	for (int i = 0; i < levelObjects.size(); i++)
	{
		levelObjects[i]->Draw(drawManager);
	}

	drawManager->Present(camera, sf::Color::White, &rTex);
	drawManager->ClearBuffer();

	if (isDebug)
	{
		for (int i = 0; i < enemies.size(); i++)
		{
			enemies[i]->GetCollisionPolygon()->DebugDraw(drawManager, &rTex);
		}
		for (int i = 0; i < levelObjects.size(); i++)
		{
			levelObjects[i]->GetCollisionPolygon()->DebugDraw(drawManager, &rTex);
		}
		for (int i = 0; i < projectilesPlayer.size(); i++)
		{
			projectilesPlayer[i].GetCollisionPolygon()->DebugDraw(drawManager, &rTex);
		}
		for (int i = 0; i < projectilesFire.size(); i++)
		{
			projectilesFire[i].GetCollisionPolygon()->DebugDraw(drawManager, &rTex);
		}
		player->GetCollisionPolygon()->DebugDraw(drawManager, &rTex);
	}

	rTex.setView(window->getDefaultView());
	rTex.draw(fire, &fireShader);

	rTex.display();
	sf::Sprite grayscaleSprite(rTex.getTexture());

	float saturation = (player->RemainingHealth() / 100.0f);
	if (saturation > 1.0f)
		saturation = 1.0f;
	if (saturation < 0.0f)
		saturation = 0.0f;

	grayShader.setParameter("grayscale", saturation);
	grayShader.setParameter("backbuffer", *grayscaleSprite.getTexture());
	grayShader.setParameter("contrast", 1.0f);
	grayShader.setParameter("brightness", 1.0f);

	sf::RenderTexture rTex2;
	rTex2.create(window->getSize().x, window->getSize().y);
	
	rTex2.setView(sf::View(window->getDefaultView()));
	rTex2.draw(grayscaleSprite, &grayShader);

	rTex2.setView(hud->GetHUDView());

	hud->Draw(drawManager);
	drawManager->Present(sf::Color::White, &rTex2);
	drawManager->ClearBuffer();
	rTex2.draw(hud->GetClockText());
	if (isPause)
	{
		rTex2.draw(*pause);
		rTex2.draw(*pauseOptions);
	}
	if(isDebug)
		hud->DebugDraw(drawManager);

	rTex2.display();
	sf::Sprite buzzSprite(rTex2.getTexture());
	buzzShader.setParameter("backbuffer", *buzzSprite.getTexture());
	buzzShader.setParameter("amount", 0.1f);
	buzzShader.setParameter("time", rand());
	window->setView(window->getDefaultView());
	window->draw(buzzSprite, &buzzShader);
	window->setView(hud->GetHUDView());
	if (isDebug)
		hud->DebugDraw(drawManager);
	window->display();
}

IState* GameState::NextState()
{
	if (backToMenu == true)
	{
		state = new MenuState;

		return state;
	}
	state = new EndState(Win);

	return state;
}

void GameState::Populate()
{
#pragma region Backdrop
	{	
		//Row 1
		{
			int row = 0;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_01.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_02.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_03.png");
				sprite->setPosition(2 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_04.png");
				sprite->setPosition(3 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_05.png");
				sprite->setPosition(4 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_06.png");
				sprite->setPosition(5 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_07.png");
				sprite->setPosition(6 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_08.png");
				sprite->setPosition(7 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_09.png");
				sprite->setPosition(8 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_10.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}

		//Row 2
		{
			int row = 1;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_11.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_12.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_13.png");
				sprite->setPosition(2 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_14.png");
				sprite->setPosition(3 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_15.png");
				sprite->setPosition(4 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_16.png");
				sprite->setPosition(5 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_17.png");
				sprite->setPosition(6 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_18.png");
				sprite->setPosition(7 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_19.png");
				sprite->setPosition(8 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_20.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}

		//Row 3
		{
			int row = 2;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_21.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_22.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_23.png");
				sprite->setPosition(2 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_26.png");
				sprite->setPosition(5 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_28.png");
				sprite->setPosition(7 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_29.png");
				sprite->setPosition(8 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_30.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}

		//Row 4
		{
			int row = 3;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_31.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_32.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_33.png");
				sprite->setPosition(2 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_39.png");
				sprite->setPosition(8 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_40.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}

		//Row 5
		{
			int row = 4;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_41.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_42.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_43.png");
				sprite->setPosition(2 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_50.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}

		//Row 6
		{
			int row = 5;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_51.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_52.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_60.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}

		//Row 7
		{
			int row = 6;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_61.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_62.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_60.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}

		//Row 8
		{
			int row = 7;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_71.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_72.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_73.png");
				sprite->setPosition(2 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_74.png");
				sprite->setPosition(3 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_75.png");
				sprite->setPosition(4 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_78.png");
				sprite->setPosition(7 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_79.png");
				sprite->setPosition(8 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_80.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}

		//Row 9
		{
			int row = 8;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_81.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_82.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_83.png");
				sprite->setPosition(2 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_84.png");
				sprite->setPosition(3 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_85.png");
				sprite->setPosition(4 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_86.png");
				sprite->setPosition(5 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_87.png");
				sprite->setPosition(6 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_88.png");
				sprite->setPosition(7 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_89.png");
				sprite->setPosition(8 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_90.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}

		//Row 10
		{
			int row = 9;
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_91.png");
				sprite->setPosition(0 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_92.png");
				sprite->setPosition(1 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_93.png");
				sprite->setPosition(2 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_94.png");
				sprite->setPosition(3 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_95.png");
				sprite->setPosition(4 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_96.png");
				sprite->setPosition(5 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_97.png");
				sprite->setPosition(6 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_98.png");
				sprite->setPosition(7 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_99.png");
				sprite->setPosition(8 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
			{
				sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\BG_tiles\\level_1_100.png");
				sprite->setPosition(9 * sprite->getLocalBounds().width, row * sprite->getLocalBounds().height);
				backdrop.push_back(sprite);
			}
		}
	}
#pragma endregion
	
	/*for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\LEVEL_1_env.png", 1120 * j, 780 * i, 1120, 780);
			sprite->setPosition(1120 * j, 780 * i);
			backdrop.push_back(sprite);
		}
	}*/
	
#pragma region Tents
	{
		float zBuffer = 0.4f;
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_tent_1.png");
			GameObject* object = new GameObject(sprite, zBuffer, 2200, 5574, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-1.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_tent_1.png");
			GameObject* object = new GameObject(sprite, zBuffer, 4290, 2075, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-14.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_tent_2.png");
			GameObject* object = new GameObject(sprite, zBuffer, 5724, 2058, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(7.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_tent_1.png");
			GameObject* object = new GameObject(sprite, zBuffer, 7490, 2093, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-4.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_tent_2.png");
			GameObject* object = new GameObject(sprite, zBuffer, 9655, 4196, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-2.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_tent_2.png");
			GameObject* object = new GameObject(sprite, zBuffer, 9778, 5317, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(9.0f);
			levelObjects.push_back(object);
		}
	}
#pragma endregion

#pragma region ConcreteSlabs
	{
		float zBuffer = 0.3f;
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 1850, 4893, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(88.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 1837, 4478, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(93.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 2067, 4081, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-40.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 2014, 3780, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-83.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 2623, 2340, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-75.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 2720, 1987, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-40.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 3118, 1872, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-35.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 3506, 1819, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(3.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 5000, 1740, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-2.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 6466, 1713, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-40.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 6740, 1916, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(45.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 8210, 2429, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(10.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 8542, 2632, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(42.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 8869, 2897, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(44.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 9125, 3153, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(48.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 9390, 3445, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(52.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 10291, 6201, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-78.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_betongsugga.png");
			GameObject* object = new GameObject(sprite, zBuffer, 10476, 6528, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(60.0f);
			levelObjects.push_back(object);
		}
	}
#pragma endregion

#pragma region InvisibleWalls
	{
		float zBuffer = 10.0f;
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\empty.png");
			sprite->setScale(12000, 2100);
			GameObject* object = new GameObject(sprite, zBuffer, 5500, 7675, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getGlobalBounds().width, object->GetSprite()->getGlobalBounds().height);
			object->SetCollisionPolygon(polygon);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\empty.png");
			GameObject* object = new GameObject(sprite, zBuffer, 10000, 6200, Center);
			CollisionPolygon* polygon = new CollisionPolygon(sf::Vector2f(10000, 6200));
			polygon->AddPoint(-1553, 425);
			polygon->AddPoint(429, 425);
			polygon->AddPoint(-1553, -258);
			polygon->AddPoint(429, -310);
			object->SetCollisionPolygon(polygon);
			levelObjects.push_back(object);
			collisionHandler.AddPolygon(polygon);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\empty.png");
			sprite->setScale(4700, 600);
			GameObject* object = new GameObject(sprite, zBuffer, 2707, 6410, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getGlobalBounds().width, object->GetSprite()->getGlobalBounds().height);
			object->SetCollisionPolygon(polygon);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\empty.png");
			GameObject* object = new GameObject(sprite, zBuffer, 2000, 3000, Center);
			CollisionPolygon* polygon = new CollisionPolygon(sf::Vector2f(2000, 3000));
			polygon->AddPoint(-164, -3000);
			polygon->AddPoint(437, -3000);
			polygon->AddPoint(-164, 569);
			polygon->AddPoint(437, 444);
			object->SetCollisionPolygon(polygon);
			levelObjects.push_back(object);
			collisionHandler.AddPolygon(polygon);
		}
	}
#pragma endregion

#pragma region Cars
	{
		float zBuffer = 0.8f;
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_brokencar.png");
			GameObject* object = new GameObject(sprite, zBuffer, 1670, 3930, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(-60.0f);
			levelObjects.push_back(object);
		}
		{
			sf::Sprite* sprite = spriteManager->CreateSprite("..\\resources\\level1\\OBJ_brokencar_blue.png");
			GameObject* object = new GameObject(sprite, zBuffer, 10700, 6030, Center);
			CollisionPolygon* polygon = collisionHandler.CreatePolygon(object->GetPosition(), Rectangular, object->GetSprite()->getLocalBounds().width, object->GetSprite()->getLocalBounds().height);
			object->SetCollisionPolygon(polygon);
			object->Rotate(120.0f);
			levelObjects.push_back(object);
		}
	}
#pragma endregion

	for (int i = 0; i < levelObjects.size(); i++)
	{
		levelObjects[i]->GetCollisionPolygon()->GiftWrap();
	}
}
