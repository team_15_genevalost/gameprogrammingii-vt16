#include "SpriteManager.hpp"

SpriteManager::SpriteManager()
{
}

SpriteManager::~SpriteManager()
{
	{
		auto it = spriteMaster.begin();
		while (it != spriteMaster.end())
		{
			delete (*it);
			it++;
		}
	}
	spriteMaster.clear();
	{
		auto it = textureMap.begin();
		while (it != textureMap.end())
		{
			delete (it->second);
			it++;
		}
	}
	textureMap.clear();
}

sf::Sprite* SpriteManager::CreateSprite(std::string fileName, int x, int y, int width, int height)
{
	sf::Texture* texture;
	sf::Sprite* sprite = new sf::Sprite();

	auto it = textureMap.find(fileName);
	if (it == textureMap.end())
	{
		texture = new sf::Texture();
		if (!texture->loadFromFile(fileName))
		{
			//Error code here
			return nullptr;
		}
		textureMap.insert(std::pair<std::string, sf::Texture*>(fileName, texture));
	}

	it = textureMap.find(fileName);
	texture = it->second;

	width = width > 0 ? width : texture->getSize().x;
	height = height > 0 ? height : texture->getSize().y;
	sf::IntRect rectangle = sf::IntRect(x, y, width, height);
	texture->setSmooth(true);
	sprite->setTexture(*texture);
	sprite->setTextureRect(rectangle);
	spriteMaster.push_back(sprite);

	return sprite;
}

AnimatedSprite* SpriteManager::CreateSprite(std::string fileName, int xCount, int yCount, float updateFrequency, AnimationType animType)
{
	sf::Texture* texture;

	auto it = textureMap.find(fileName);
	if (it == textureMap.end())
	{
		texture = new sf::Texture();
		if (!texture->loadFromFile(fileName))
		{
			//Error code here
			return nullptr;
		}
		textureMap.insert(std::pair<std::string, sf::Texture*>(fileName, texture));
	}

	it = textureMap.find(fileName);
	texture = it->second;

	texture->setSmooth(true);
	AnimatedSprite* animatedSprite = new AnimatedSprite(*texture, xCount, yCount, animType);
	animatedSprite->SetUpdateFrequency(updateFrequency);
	spriteMaster.push_back(animatedSprite);

	return animatedSprite;
}

int SpriteManager::DestroySprite(sf::Sprite* sprite)
{
	auto it = spriteMaster.begin();
	while (it != spriteMaster.end())
	{
		if ((*it) == sprite)
		{
			delete (*it);
			spriteMaster.erase(it);
			return 0;
		}
		it++;
	}
	return -1;
}
