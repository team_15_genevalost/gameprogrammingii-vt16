#include "StateManager.hpp"

StateManager::StateManager()
{
	CurrentState = nullptr;
}

StateManager::~StateManager()
{

	if (CurrentState != nullptr)
	{
		CurrentState->Exit();
		delete CurrentState;
		CurrentState = nullptr;
	}
}

bool StateManager::Update(float deltaTime)
{
	if (CurrentState != nullptr)
	{
		if (CurrentState->Update(deltaTime) == false)
		{
			SetState(CurrentState->NextState());
		}
	}
	return true;
}

void StateManager::Draw()
{
	if (CurrentState != nullptr)
	{
		CurrentState->Draw();
	}
}

void StateManager::SetState(IState* nextState)
{

	if (CurrentState != nullptr)
	{
		CurrentState->Exit();
		delete CurrentState;
		CurrentState = nullptr;
	}

	CurrentState = nextState;
	CurrentState->Enter();
}
