#include "CollisionCircle.hpp"

CollisionCircle::CollisionCircle(sf::Vector2f position, float radius, sf::Vector2f origin) : CollisionPrimitive(position, origin)
{
	this->radius = radius;
	localCenter = { 0, 0 };
	UpdateCenter();
}

float CollisionCircle::GetRadius()
{
	return radius;
}

void CollisionCircle::SetRadius(float radius)
{
	this->radius = radius;
}

sf::Vector2f CollisionCircle::GetCenter()
{
	return localCenter + position;
}

sf::FloatRect CollisionCircle::GetGlobalBounds()
{
	sf::FloatRect rect;
	rect.left = localCenter.x + position.x - radius;
	rect.top = localCenter.y + position.y - radius;
	rect.height = radius * 2;
	rect.width = radius * 2;
	return rect;
}

void CollisionCircle::SetRotation(float rotation)
{
	CollisionPrimitive::SetRotation(rotation);
	UpdateCenter();
}

void CollisionCircle::Rotate(float rotation)
{
	CollisionPrimitive::Rotate(rotation);
	UpdateCenter();
}

void CollisionCircle::DebugDraw(DrawManager* drawManager)
{
	sf::CircleShape circle;
	circle.setRadius(radius);
	circle.setPosition(GetCenter());
	circle.setFillColor(sf::Color(0, 0, 0, 0));
	circle.setOutlineColor(sf::Color(255, 255, 0, 128));
	circle.setOutlineThickness(-2.0f);
	circle.setOrigin(radius + origin.x, radius + origin.y);
	drawManager->Draw(&circle);
}

void CollisionCircle::UpdateCenter()
{
	if (origin == localCenter)
	{
		return;
	}
	float angle = my::math::DegToRad(rotation);
	my::vmath::RotateAroundOrigin(&localCenter.x, &localCenter.y, angle);
}