#include "..\\include\Player.hpp"

Player::Player(sf::Sprite* torso, sf::Sprite* legs, sf::Sprite* lights, sf::Sprite* shootTorso, sf::Sprite* shootLight, float zBuffer, sf::Vector2f position, float angle, sf::Vector2f origin) : Entity(torso, zBuffer, position, angle, origin)
{
	moveSpeed = 750.0f;
	currentHealth = MaxHealth;
	regenerationTimer = RegenerationDelay;

	this->legs = legs;
	this->lightsWalk = lights;
	this->lightsShoot = shootLight;
	this->torsoWalk = torso;
	this->torsoShoot = shootTorso;

	legs->setOrigin(torso->getOrigin());
	lightsWalk->setOrigin(torso->getOrigin());
	lightsShoot->setOrigin(torso->getOrigin());
	torsoShoot->setOrigin(torso->getOrigin());

	this->lights = lightsWalk;
	previousPosition = position;
	isAlive = true;

	fireRateTimer = 0;
	fireDelayTimer = 0;
	burstQueue = 0;
	projectileQueue = 0;
	firePowerDelayTimer = 0;
	firePower = MaxFirePower;
	firePowerOverflow = 0;
}

Player::~Player()
{

}

void Player::Update(float deltaTime)
{
	if (isAlive)
	{
		previousPosition = sprite->getPosition();
		float cosine = cos(my::math::DegToRad(sprite->getRotation()));
		float sine = sin(my::math::DegToRad(sprite->getRotation()));

		polygon->SetPosition(sprite->getPosition());
		polygon->Update();

		sf::Vector2f heading = { 0, 0 };

		if (sf::Keyboard().isKeyPressed(sf::Keyboard().W))
		{
			heading += sf::Vector2f(0, -1);
		}
		if (sf::Keyboard().isKeyPressed(sf::Keyboard().S))
		{
			heading += sf::Vector2f(0, 1);
		}
		if (sf::Keyboard().isKeyPressed(sf::Keyboard().A))
		{
			heading += sf::Vector2f(-1, 0);
		}
		if (sf::Keyboard().isKeyPressed(sf::Keyboard().D))
		{
			heading += sf::Vector2f(1, 0);
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && fireDelayTimer >= fireDelay)
		{
			sprite = torsoShoot;
			lights = lightsShoot;
			static_cast<AnimatedSprite*>(torsoShoot)->Reset();
			static_cast<AnimatedSprite*>(lightsShoot)->Reset();
			projectileQueue++;
			fireDelayTimer = 0;
			fireRateTimer = 0;
			burstQueue = 2;
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
		{
			float fireConsumption = FirePowerRate * deltaTime < firePower ? FirePowerRate * deltaTime : firePower;
			firePower -= fireConsumption;
			int amount = fireConsumption;
			firePowerOverflow += fireConsumption - amount;
			amount += firePowerOverflow;
			firePowerOverflow -= static_cast<int>(firePowerOverflow);
			firePowerQueue += amount;
			firePowerDelayTimer = 0;
		}

		firePowerDelayTimer += deltaTime;
		if (firePowerDelayTimer >= FirePowerDelay)
		{
			firePower += FirePowerRegeneration * deltaTime;
		}

		if (firePower > MaxFirePower)
		{
			firePower = MaxFirePower;
		}

		if (burstQueue > 0)
		{
			if (fireRateTimer >= 1.0f / fireRate)
			{
				fireRateTimer = 0;
				burstQueue--;
				projectileQueue++;
			}
			fireRateTimer += deltaTime;
		}

		my::vmath::Normalize(&heading.x, &heading.y);
		heading *= moveSpeed * deltaTime;

		Move(heading);

		sf::Color lightColor = sf::Color(255, 255 - 145 * firePower / MaxFirePower, 255 - 255 * firePower / MaxFirePower);

		lightsWalk->setRotation(sprite->getRotation());
		lightsWalk->setPosition(sprite->getPosition());
		lightsWalk->setColor(lightColor);

		lightsShoot->setRotation(sprite->getRotation());
		lightsShoot->setPosition(sprite->getPosition());
		lightsShoot->setColor(lightColor);

		legs->setRotation(sprite->getRotation());
		legs->setPosition(sprite->getPosition());

		torsoWalk->setRotation(sprite->getRotation());
		torsoWalk->setPosition(sprite->getPosition());

		torsoShoot->setRotation(sprite->getRotation());
		torsoShoot->setPosition(sprite->getPosition());

		float sign = my::math::Sign(my::vmath::DotProduct(heading.x, heading.y, cosine, sine));

		static_cast<AnimatedSprite*>(legs)->Update(deltaTime * sign);
		int n = static_cast<AnimatedSprite*>(legs)->CurrentFrame();
		static_cast<AnimatedSprite*>(torsoWalk)->SetSprite(n);
		static_cast<AnimatedSprite*>(lightsWalk)->SetSprite(n);

		static_cast<AnimatedSprite*>(torsoShoot)->Update(deltaTime);
		static_cast<AnimatedSprite*>(lightsShoot)->Update(deltaTime);

		if (static_cast<AnimatedSprite*>(sprite)->HasEnded())
		{
			sprite = torsoWalk;
			lights = lightsWalk;
		}

		if (currentHealth <= 0)
		{
			isVisible = false;
			isAlive = false;
		}

		if (regenerationTimer >= RegenerationDelay)
			currentHealth += (deltaTime / RegenerationSpeed) * MaxHealth;
		if (currentHealth > MaxHealth)
			currentHealth = MaxHealth;

		regenerationTimer += deltaTime;
		fireDelayTimer += deltaTime;
	}
}

bool Player::IsVisible()
{

	return false;
}

void Player::Damage(float amount)
{
	currentHealth -= amount;
	regenerationTimer = 0;
}

float Player::RemainingHealth()
{
	return currentHealth;
}

bool Player::IsAlive()
{
	return isAlive;
}

int Player::GetProjectileQueue()
{
	int queue = projectileQueue;
	projectileQueue = 0;
	return queue;
}

int Player::GetFirePowerQueue()
{
	int queue = firePowerQueue;
	firePowerQueue = 0;
	return queue;
}

void Player::Draw(DrawManager* drawManager)
{
	drawManager->DrawCall(sprite, zBuffer);
	drawManager->DrawCall(legs, zBuffer - 0.0001f);
	drawManager->DrawCall(lights, zBuffer + 0.0001f);
}

sf::Vector2f Player::GetPreviousPosition()
{
	return previousPosition;
}
