uniform float time;
uniform vec2 resolution;
uniform sampler2D backbuffer;
uniform float amount;

float snoise(vec2 v){
    return fract(sin(dot(v.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
    vec2 position = ( gl_FragCoord.xy / resolution.xy );
    vec4 color = texture2D(backbuffer, position);
    if(color.a <= 0.1)
    {
    	gl_FragColor = color;
    }
    else
    {
	    float rand = snoise(vec2(position.x * cos(time), position.y * sin(time))); 
		gl_FragColor.r = mix(color.x, rand, amount);
		gl_FragColor.g = mix(color.y, rand, amount);
		gl_FragColor.b = mix(color.z, rand, amount);
		gl_FragColor.a = color.w;
	}
}