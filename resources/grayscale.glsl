uniform vec2 resolution;
uniform sampler2D backbuffer;
uniform float grayscale;
uniform float contrast;
uniform float brightness;

void main()
{
    vec2 position = ( gl_FragCoord.xy / resolution.xy );
    vec4 color = texture2D(backbuffer, position);
    vec4 white;
    white = vec4(1, 1, 1, 1);
    vec4 contrastColor = ((color - (white * 0.5)) * contrast) + (white * 0.5);
    float gray = (contrastColor.x + contrastColor.y + contrastColor.z) / 3;
    gl_FragColor.r = mix(gray, contrastColor.x, grayscale) * brightness;
    gl_FragColor.g = mix(gray, contrastColor.y, grayscale) * brightness;
    gl_FragColor.b = mix(gray, contrastColor.z, grayscale) * brightness;
    gl_FragColor.a = 1.0;
}