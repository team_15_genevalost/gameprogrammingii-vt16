uniform sampler2D backbuffer;
uniform sampler2D fire;
uniform vec2 resolution;

void main()
{
	vec2 position = ( gl_FragCoord.xy / resolution.xy );
	vec4 color = texture2D(backbuffer, position);
	vec4 fireColor = texture2D(fire, position);
	gl_FragColor = fireColor;
	gl_FragColor.a = color.x;
}