#pragma once
#include "Include.hpp"

class Camera
{
public:
	Camera(sf::Vector2f position, float width, float height, sf::Vector2f* cameraFocus = nullptr, float focus = 0.0f);
	void Update(sf::Vector2f position);
	sf::Vector2f GetCenter();
	sf::Vector2f GetPosition();
	sf::FloatRect GetCameraRect();
	void AddOffset(sf::Vector2f offset);
	void AddOffset(float x, float y);
	sf::View GetCameraView();
	void Zoom(float amount);
	void SetZoom(float zoom);
	float GetZoom();
	~Camera();
private:
	sf::Vector2f position;
	sf::Vector2f* cameraFocus;
	sf::View cameraView;
	sf::Vector2f cameraOffset;
	float width;
	float height;
	float focus;
	float zoom;
	Camera();
};