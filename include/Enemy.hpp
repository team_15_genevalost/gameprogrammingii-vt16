#pragma once

#include "Include.hpp"
#include "AnimatedSprite.hpp"
#include "DrawManager.hpp"
#include "Entity.hpp"

class Enemy : public Entity
{
public:	
	Enemy(AnimatedSprite* walkAnimation, AnimatedSprite* attackAnimation, float zBuffer, sf::Vector2f position, sf::Vector2f origin = sf::Vector2f(0,0), float angle = .0f);
	Enemy(AnimatedSprite* walkAnimation, AnimatedSprite* attackAnimation, float zBuffer, sf::Vector2f position, Origin origin, float angle = .0f);
	~Enemy();	
	void Update(float deltaTime);
	void Damage(float amount);
	void UpdateMovement();
	bool IsAlive();
	void SetTargetPosition(sf::Vector2f* position);
	void SetAttackAnimation(AnimatedSprite* animation);

private:
	float moveSpeed;
	float rotationSpeed;
	float health;
	sf::Vector2f* targetPosition;
	AnimatedSprite* walkAnimation;
	AnimatedSprite* attackAnimation;
};