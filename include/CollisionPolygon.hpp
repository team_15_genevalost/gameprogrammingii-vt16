#pragma once
#include "CollisionPrimitive.hpp"

class CollisionPolygon : public CollisionPrimitive
{
public:
	CollisionPolygon(sf::Vector2f position, sf::Vector2f origin = sf::Vector2f(0.0f, 0.0f));
	sf::FloatRect GetGlobalBounds();
	sf::FloatRect GetLocalBounds();
	sf::Vector2f GetPoint(int n);
	void DeletePoint(int n);
	std::vector<sf::Vector2f> GetPoints();
	void AddPoint(sf::Vector2f vertex);
	void AddPoint(float x, float y);
	int NrOfPoints();
	void GiftWrap();
	void Update();
	void FlipHorizontal();
	void FlipVertical();
	bool IsValid();
	void RemovePoint(int n);
	void MovePoint(int n, sf::Vector2f move);
	void SetPoint(int n, sf::Vector2f point);
	sf::Vector2f GetCenter();
	void SetRotation(float rotation);
	void Rotate(float rotation);
	void SetOrigin(sf::Vector2f origin);
	void DebugDraw(DrawManager* drawManager, sf::RenderTexture* renderTexture = nullptr);
private:
	std::vector<sf::Vector2f> vertices;
	bool isValid;
	bool isTested;
};