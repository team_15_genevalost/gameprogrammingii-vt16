#pragma once
#include "Include.hpp"
#include "IState.hpp"
#include "MenuState.hpp"
#include "SpriteManager.hpp"
#include "HighScore.hpp"

class EndState : public IState
{
public:
	EndState(bool Win);
	~EndState();
	void Draw();
	void Enter(sf::RenderWindow* window);
	void Exit();

	bool Update(float deltaTIme);

	IState* NextState();

private:

	HighScore oScore1, oScore2;


	sf::View view;
	sf::Vector2f* mousePosition;
	sf::RenderWindow* window;
	sf::Sprite* LossCondition;
	sf::Sprite* VictoryCondition;
	sf::Sprite* returnToMenu;
	sf::Sprite* menuExit;
	sf::Sprite* reticle;
	sf::Sprite* enterInitials;

	sf::Mouse mouse;

	sf::Event event;
	std::string str;
	sf::String* text;

	IState* state;
	System m_xSystem;
	SpriteManager* spriteManager;
	DrawManager* drawManager;

	bool checkWin;
};