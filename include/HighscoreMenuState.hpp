#pragma once
#include "Include.hpp"
#include "IState.hpp"
#include "GameState.hpp"
#include "SpriteManager.hpp"
#include "HighScore.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>

class HighscoreMenuState : public IState
{
public:
	HighscoreMenuState();
	~HighscoreMenuState();
	void Draw();
	void Enter(sf::RenderWindow* window);
	void Exit();

	bool Update(float deltaTIme);

	IState* NextState();

private:
	HighScore oScore1, oScore2;
	sf::View view;
	sf::Vector2f* mousePosition;
	sf::RenderWindow* window;
	sf::Sprite* GUI_Highscore;
	sf::Sprite* highscoreBack;
	sf::Sprite* reticle;
	sf::Mouse mouse;

	sf::Text HighScore;
	sf::Text SecondScore;
	sf::Text ThirdScore;
	sf::Text ForthScore;
	sf::Text FifthScore;
	sf::Text SixthScore;
	sf::Text SeventhScore;
	sf::Text EighthScore;
	sf::Text NinthScore;
	sf::Text TenthScore;

	std::ifstream streamin;
	std::string line[20];
	sf::Font font;

	IState* state;
	System m_xSystem;
	SpriteManager* spriteManager;
	DrawManager* drawManager;
};