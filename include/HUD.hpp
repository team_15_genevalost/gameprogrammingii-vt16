#pragma once

#include "Include.hpp"
#include "Player.hpp"
#include "SpriteManager.hpp"
#include "DrawManager.hpp"
#include "AnimatedSprite.hpp"
#include <iostream>
#include <sstream>
#include <iomanip>

class HUD
{
public:
	HUD(SpriteManager* spriteManager, float sizeX, float sizeY);
	void Update(float deltaTime);
	void Draw(DrawManager* drawManager);
	void SetReticlePosition(float x, float y);
	void SetReticlePosition(sf::Vector2f position);
	void SetCenterBoxPosition(float x, float y);
	void SetCenterBoxPosition(sf::Vector2f position);
	sf::View GetHUDView();
	sf::Text GetClockText();
	void DebugUpdate(float deltaTime);
	void DebugDraw(DrawManager* drawManager);
	float GetClock();

private:
	sf::Sprite* frameShade;
	sf::Sprite* centerBox;
	sf::Sprite* frame;
	sf::Sprite* reticle;
	sf::Vector2f centerBoxPosition;
	sf::View HUDView;

	sf::RectangleShape* debugBar;

	float centerBoxMoveSpeed;

	AnimatedSprite* textScroll;

	float clock;

	int* lastFPS;
	int lastFPSSize;

	sf::Text debugFPS;
	sf::Font debugFont;

	sf::Text clockText;
	sf::Font clockFont;
};