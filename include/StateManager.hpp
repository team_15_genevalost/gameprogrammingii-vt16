#pragma once
#include "Include.hpp"
#include "IState.hpp"

class StateManager
{
	friend class Engine;
public:
	StateManager();
	~StateManager();
	bool Update(float deltaTime);
	void Draw();
private:
	void SetState(IState* State);
	IState* CurrentState;
};