#pragma once

#include "SpriteManager.hpp"
#include "DrawManager.hpp"


struct System
{
	int ScreenWidth;
	int ScreenHeight;
	SpriteManager* spriteManager;
	DrawManager* drawManager;
};

class IState
{
public:
	virtual void Enter(sf::RenderWindow* window) = 0;
	virtual bool Update(float DeltaTime) = 0;
	virtual void Exit() = 0;
	virtual void Draw() = 0;
	virtual IState* NextState() = 0;

protected:
	const int WindowSizeX = 1920;
	const int WindowSizeY = 1080;

	float scaleX;
	float scaleY;
};