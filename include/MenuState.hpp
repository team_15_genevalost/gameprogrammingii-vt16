#pragma once
#include "Include.hpp"
#include "IState.hpp"
#include "GameState.hpp"
#include "HighscoreMenuState.hpp"
#include "SpriteManager.hpp"

class MenuState : public IState
{
public:
	MenuState();
	~MenuState();
	void Draw();
	void Enter(sf::RenderWindow* window);
	void Exit();
	bool isHighscoreButtonPressed;
	
	bool Update(float deltaTIme);

	IState* NextState();

private:
	sf::View view;
	sf::Vector2f* mousePosition;
	sf::RenderWindow* window;
	sf::Sprite* menuGloben;
	sf::Sprite* menuLogo;
	sf::Sprite* menuStart;
	sf::Sprite* menuOptions;
	sf::Sprite* highscoreMenuButton;
	sf::Sprite* menuExit;
	sf::Sprite* reticle;
	sf::Mouse mouse;
	
	IState* state;
	System m_xSystem;
	SpriteManager* spriteManager;
	DrawManager* drawManager;
};