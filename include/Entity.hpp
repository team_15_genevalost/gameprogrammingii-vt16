#pragma once
#include "Include.hpp"
#include "GameObject.hpp"
#include "AnimatedSprite.hpp"
#include "CollisionPolygon.hpp"

class Entity : public GameObject
{
public:	
	Entity(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, float angle = .0f, sf::Vector2f origin = sf::Vector2f(.0f, .0f));
	Entity(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, Origin origin, float angle = .0f);
	Entity(sf::Sprite* sprite, float zBuffer, float x, float y, float angle = .0f, float originX = .0f, float originY = .0f);
	Entity(sf::Sprite* sprite, float zBuffer, float x, float y, Origin origin, float angle = .0f);

	virtual void Update(float deltaTime);
	~Entity();
};