#pragma once
#include "Include.hpp"
#include "AnimatedSprite.hpp"

class SpriteManager
{
public:
	SpriteManager();
	~SpriteManager();
	sf::Sprite* CreateSprite(std::string fileName, int x = 0, int y = 0, int width = 0, int height = 0);
	AnimatedSprite* CreateSprite(std::string fileName, int xCount, int yCount, float updateFrequency, AnimationType animType = Looping);
	int DestroySprite(sf::Sprite* sprite);
private:
	std::vector<sf::Sprite*> spriteMaster;
	std::map<std::string, sf::Texture*> textureMap;

};