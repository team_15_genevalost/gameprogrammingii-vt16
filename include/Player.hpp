#pragma once
#include "Include.hpp"
#include "Entity.hpp"
#include "DrawManager.hpp"
#include "SpriteManager.hpp"
#include "CollisionHandler.hpp"
#include "AnimatedSprite.hpp"
#include "Projectile.hpp"

#include <cmath>

class Player : public Entity
{
public:
	Player(sf::Sprite* torso, sf::Sprite* legs, sf::Sprite* lights, sf::Sprite* shootTorso, sf::Sprite* shootLight, float zBuffer, sf::Vector2f position, float angle = .0f, sf::Vector2f origin = sf::Vector2f(.0f, .0f));
	~Player();

	void Update(float deltaTime);
	bool IsVisible();	
	void Damage(float amount);	
	float RemainingHealth();
	bool IsAlive();
	int GetProjectileQueue();
	int GetFirePowerQueue();
	void Draw(DrawManager* drawManage);

	sf::Vector2f GetPreviousPosition();

private:
	const float MaxHealth = 100.0f;
	const float RegenerationSpeed = 10.0f;
	const float RegenerationDelay = 5.0f;
	const float fireRate = 45.0f;
	const float fireDelay = .75f;
	const float MaxFirePower = 15.0f;
	const float FirePowerRate = 10.0f;
	const float FirePowerDelay = 5.0f;
	const float FirePowerRegeneration = 4.5f;

	float firePowerOverflow;
	float firePower;
	float firePowerDelayTimer;
	float fireRateTimer;
	float fireDelayTimer;
	float regenerationTimer;
	float moveSpeed;
	float remainingHealth;
	float currentHealth;

	int projectileQueue;
	int firePowerQueue;

	int burstQueue;

	bool isAlive;

	sf::Sprite* legs;
	sf::Sprite* lights;
	sf::Sprite* lightsShoot;
	sf::Sprite* lightsWalk;
	sf::Sprite* torsoShoot;
	sf::Sprite* torsoWalk;

	sf::Vector2f previousPosition;

	CollisionPolygon* collisionPolygon;
};
