#pragma once
#include "Include.hpp"

enum AnimationType
{
	Looping,
	BackAndForth,
	RunOnce,
	Random,
	ReverseLooping,
	ReverseRunOnce
};

class AnimatedSprite : public sf::Sprite
{
public:
	AnimatedSprite(const sf::Texture &texture, int xCount, int yCount, AnimationType animType = Looping);
	void SetUpdateFrequency(float val);
	float GetUpdateFrequency();
	void Update(float DeltaTime);
	int CurrentFrame();
	bool HasEnded();
	void Reset();
	void SetSprite(int n);
	int GetNrOfFrames();
	~AnimatedSprite();
	
private:
	void NextSprite();
	void PreviousSprite();
	void RandomSprite();
	float updateFrequency;
	float accumulator;
	AnimationType animType;
	bool hasEnded;

	AnimatedSprite() {}
};
