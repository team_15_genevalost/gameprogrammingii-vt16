#pragma once

#define _USE_MATH_DEFINES

#include <cmath>
#include <limits>
#include <Windows.h>

namespace my
{
	namespace vmath
	{
		float DotProduct(float x1, float y1, float x2, float y2);
		float Distance(float x1, float y1, float x2, float y2);
		void Normalize(float* x, float* y);
		void GetUnit(float x, float y, float* targetX, float* targetY);
		float GetLength(float x, float y);
		void RotateAroundOrigin(float* x, float* y, float angle);
		void RotateAroundPoint(float pointX, float pointY, float* x, float* y, float angle);
		float ScalarProjection(float pointX, float pointY, float axisX, float axisY);
		void VectorProjection(float pointX, float pointY, float axisX, float axisY, float* targetX, float* targetY);
	};

	namespace math
	{
		float Lerp(float v0, float v1, float t);
		float Wrap(float min, float max, float val);
		void Wrap(float min, float max, float* val);
		float Clip(float min, float max, float val);
		void Clip(float min, float max, float* val);
		float RadToDeg(float radians);
		float DegToRad(float degrees);	
		int Sign(float val);
		bool HasOverlap(float minA, float maxA, float minB, float maxB);
		bool Contains(float min, float max, float val);
		float fRand(float min, float max);
		float fRand(float range);
	};
}