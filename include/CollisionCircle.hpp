#pragma once
#include "CollisionPrimitive.hpp"

class CollisionCircle :public CollisionPrimitive
{
public:
	CollisionCircle(sf::Vector2f position, float radius, sf::Vector2f origin = sf::Vector2f(0.0f, 0.0f));
	float GetRadius();
	void SetRadius(float p_fRadius);
	sf::Vector2f GetCenter();
	void Update() {};
	sf::FloatRect GetGlobalBounds();
	void SetRotation(float rotation);
	void Rotate(float rotation);
	void DebugDraw(DrawManager* drawManager);
protected:
	float radius;
	sf::Vector2f localCenter;
	void UpdateCenter();
private:
	CollisionCircle() {}
};