#pragma once
#include "Include.hpp"
#include "Entity.hpp"

class Projectile : public Entity
{
public:
	Projectile(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, float angle = .0f, float tll = 0, float speed = 0, float damage = 0, sf::Vector2f origin = sf::Vector2f(.0f, .0f));
	Projectile(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, Origin origin, float angle = .0f, float tll = 0, float speed = 0, float damage = 0);
	Projectile(sf::Sprite* sprite, float zBuffer, float x, float y, float angle = .0f, float tll = 0, float speed = 0, float damage = 0, float originX = .0f, float originY = .0f);
	Projectile(sf::Sprite* sprite, float zBuffer, float x, float y, Origin origin, float angle = .0f, float tll = 0, float speed = 0, float damage = 0);
	int GetDamage();
	bool IsDestroyed();
	void Update(float deltaTime);

private:
	void Create(float ttl, float speed, float damage);
	float ttl;
	float speed;
	int damage;
	bool isDestroyed;
};