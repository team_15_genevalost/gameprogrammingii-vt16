#pragma once
#include "Include.hpp"
#include "DrawManager.hpp"
#include "CollisionPolygon.hpp"

enum Origin { TopLeft, TopMiddle, TopRight, MiddleLeft, Center, MiddleRight, BottomLeft, BottomMiddle, BottomRight };

class GameObject
{
public:
	GameObject(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, float angle = .0f, sf::Vector2f origin = sf::Vector2f(.0f, .0f), CollisionPolygon* polygon = nullptr);
	GameObject(sf::Sprite* sprite, float zBuffer, float x, float y, float angle = .0f, float originX = .0f, float originY = .0f, CollisionPolygon* polygon = nullptr);
	GameObject(sf::Sprite* sprite, float zBuffer, float x, float y, Origin origin, float angle = .0f, CollisionPolygon* polygon = nullptr);
	GameObject(sf::Sprite* sprite, float zBuffer, sf::Vector2f position, Origin origin, float angle = .0f, CollisionPolygon* polygon = nullptr);
	~GameObject();
	void SetSprite(sf::Sprite* sprite);
	void SetPosition(sf::Vector2f position);
	virtual void SetRotation(float angle);
	sf::Sprite* GetSprite();
	sf::Vector2f GetPosition();
	float GetRotation();
	void Move(sf::Vector2f move);
	void Move(float x, float y);
	void Rotate(float amount);
	float GetZBuffer();
	void SetZBuffer(float zBuffer);
	sf::Vector2f GetOrigin();
	void SetOrigin(sf::Vector2f origin);
	bool IsVisible();
	void SetIsVisible(bool isVisible);
	void Draw(DrawManager* drawManager);
	void SetOrigin(Origin origin);
	void SetOrigin(float x, float y);
	void SetCollisionPolygon(CollisionPolygon* polygon);
	CollisionPolygon* GetCollisionPolygon();

protected:
	CollisionPolygon* polygon;
	sf::Sprite* sprite;
	float zBuffer;
	bool isVisible;
private:
	GameObject(){}
};