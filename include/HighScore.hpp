#pragma once
#include <iostream>
#include <vector>

typedef struct {
	char *name;
	int score;
	bool free;
} score_t;

struct HighScoreItem
{
	std::string name;
	int score;
};
class HighScore {
public:
	HighScore(int el = 10);
	~HighScore();
	bool save(char *fileName);
	bool load(char *fileName);
	void print(std::ostream &out);
	void add(std::string name, int score);
	void UpdateFile();
	void LoadHighscores();

private:
	int nEl;
	score_t **v;

	std::vector<HighScoreItem*> highscoreItems;

};