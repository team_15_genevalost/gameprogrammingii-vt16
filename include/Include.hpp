#pragma once

#define _USE_MATH_DEFINES

#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

#include "Utility.hpp"