#pragma once
#include "Include.hpp"
#include "Camera.hpp"
#include "SFML\OpenGL.hpp"

class DrawManager
{
public:
	DrawManager(sf::RenderWindow* window);
	void DrawCall(sf::Sprite* sprite, float zBuffer);
	void Draw(sf::Drawable* drawable, sf::RenderTexture* renderTexture = nullptr);
	void Present(Camera* camera, sf::Color color = sf::Color::White, sf::RenderTexture* renderTexture = nullptr);
	void Present(sf::Color color = sf::Color::White, sf::RenderTexture* renderTexture = nullptr);
	void ClearBuffer();
private:
	std::vector<std::pair<float, sf::Sprite*>> buffer;
	sf::RenderWindow* window;
};