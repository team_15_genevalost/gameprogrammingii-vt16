#pragma once

#include "Include.hpp"
#include "IState.hpp"
#include "Projectile.hpp"
#include "SpriteManager.hpp"
#include "AnimatedSprite.hpp"
#include "Camera.hpp"
#include "DrawManager.hpp"
#include "Entity.hpp"
#include "Player.hpp"
#include "Enemy.hpp"
#include "MenuState.hpp"
#include <Windows.h>
#include "HUD.hpp"
#include "CollisionPolygon.hpp"
#include "CollisionHandler.hpp"
#include "EndState.hpp"
#include "HighScore.hpp"

class GameState : public IState
{
public:
	GameState();
	~GameState();
	void Enter(sf::RenderWindow* window);
	bool Update(float deltaTime);
	void Exit();
	void Draw();
	IState* NextState();

private:
	void Populate();
	
	sf::View view;
	sf::Vector2f* mousePosition;
	HighScore oScore1, oScore2;

	int minPitch;
	int maxPitch;
	bool randomPitch;
	float currentPitch;

	float timeManip;

	float scaleX;
	float scaleY;

	float projectileSpeed;
	float moveSpeed;
	float moveSpeed2;
	float rotationSpeed;

	int projectileDamage;
	int fireDamage;
	int hitPoints;
	int crawlerDamage;

	bool lastRightClick;
	bool lastLeftClick;
	bool isBloodVisible;

	bool Win;
	bool isPause;
	bool previousEscapeState;
	bool previousF3State;
	bool backToMenu;

	bool isDebug;

	std::vector<Projectile> projectilesPlayer;
	std::vector<Projectile> projectilesFire;
	std::vector<Projectile> projectileEnemies;
	std::vector<GameObject> decoLayer;
	std::vector<GameObject*> levelObjects;
	std::vector<sf::Sprite*> backdrop;
	std::vector<Enemy*> enemies;
	Player* player;
	
	sf::Sprite* death;
	sf::Sprite* background;
	sf::Sprite* background2;

	sf::Sound rifleSound;
	sf::Sound flameThrower;
	sf::Sound maleGruntSound;
	sf::SoundBuffer buffer;
	sf::SoundBuffer buffer2;
	sf::SoundBuffer buffer3;

	sf::Sprite* pause;
	sf::Sprite* pauseOptions;

	HUD* hud;

	sf::Mouse mouse;
	sf::Shader grayShader;
	sf::Shader buzzShader;
	sf::Shader fireShader;
	sf::RenderWindow* window;
	
	SpriteManager* spriteManager;
	DrawManager* drawManager;
	CollisionHandler collisionHandler;
	Camera* camera;
	IState* state;
};