#pragma once
#include "Include.hpp"
#include "DrawManager.hpp"

class CollisionPrimitive {
public:
	CollisionPrimitive(sf::Vector2f position, sf::Vector2f origin = sf::Vector2f(0.0f, 0.0f)) { this->position = position;	this->origin = origin; onHitProjection = sf::Vector2f(0.0f, 0.0f); };
	virtual sf::Vector2f GetPosition() { return position; }
	virtual sf::Vector2f GetOrigin() { return origin; }
	virtual void SetOrigin(sf::Vector2f origin) { this->origin = origin; }
	virtual sf::Vector2f GetCenter() = 0;
	float GetRotation() { return rotation; }
	virtual void SetRotation(float rotation) { this->rotation = rotation; }
	virtual void Rotate(float rotation) { this->rotation += rotation; }
	virtual void SetOnHitProjection(sf::Vector2f projection) { onHitProjection = projection; }
	virtual sf::Vector2f GetOnHitProjection() { return onHitProjection; }
	virtual void SetPosition(sf::Vector2f position) { this->position = position; }
	virtual void Move(sf::Vector2f move) { position += move; }
	virtual sf::FloatRect GetGlobalBounds() = 0;
	virtual void DebugDraw(DrawManager* drawManager, sf::RenderTexture* renderTexture = nullptr) = 0;
	virtual void Update() = 0;
	~CollisionPrimitive() {}
protected:
	CollisionPrimitive() {}
	sf::Vector2f position;
	sf::Vector2f origin;
	sf::Vector2f onHitProjection;
	float rotation;
};