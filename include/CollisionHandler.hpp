#pragma once

#include "Include.hpp"
#include "CollisionCircle.hpp"
#include "CollisionPolygon.hpp"

enum PolygonShapes { Rectangular, RightTriangle, IsoscelesTriangle };

class CollisionHandler
{
public:
	~CollisionHandler();
	CollisionPolygon* CreatePolygon(sf::Vector2f position);
	CollisionPolygon* CreatePolygon(sf::Vector2f position, int numberOfSides, float width, float height);
	CollisionPolygon* CreatePolygon(sf::Vector2f position, PolygonShapes shape, float width, float height);
	void AddPolygon(CollisionPolygon* polygon);
	static bool CollisionCheck(CollisionPrimitive* objectA, CollisionPrimitive* objectB);
	static bool Check(CollisionPolygon* objectA, CollisionPolygon* objectB);

private:
	std::vector<CollisionPolygon*> polygons;
};